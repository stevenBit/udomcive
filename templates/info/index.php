<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Theme Starz">

    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="<?php echo WEB_ROOT;?>templates/info/assets/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo WEB_ROOT;?>templates/info/assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="<?php echo WEB_ROOT;?>templates/info/assets/css/selectize.css" type="text/css">
    <link rel="stylesheet" href="<?php echo WEB_ROOT;?>templates/info/assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="<?php echo WEB_ROOT;?>templates/info/assets/css/vanillabox/vanillabox.css" type="text/css">
    <link rel="stylesheet" href="<?php echo WEB_ROOT;?>templates/info/assets/css/vanillabox/vanillabox.css" type="text/css">

    <link rel="stylesheet" href="<?php echo WEB_ROOT;?>templates/info/assets/css/style.css" type="text/css">
    <!-- DataTables CSS -->
    <link href="<?php echo WEB_ROOT;?>templates/cive/css/dataTables.bootstrap.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo WEB_ROOT;?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo WEB_ROOT;?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo WEB_ROOT;?>assets/js/scripts.js"></script>

    <title>UDOM : College of Informatics</title>

</head>


<body class="page-homepage-carousel">
<!-- Wrapper -->
<div class="wrapper">
<!-- Header -->
<div class="navigation-wrapper">
    <div class="secondary-navigation-wrapper">
        <div class="container">
          <div class="navigation-contact pull-left">  <!-- Call Us:  <span class="opacity-70">000-123-456-789</span> --></div>
            <div class="search">
                <div class="input-group">
                    <input type="search" class="form-control" name="search" placeholder="Search">
                    <span class="input-group-btn"><button type="submit" id="search-submit" class="btn"><i class="fa fa-search"></i></button></span>
                </div><!-- /.input-group -->
            </div>
            <ul class="secondary-navigation list-unstyled">
                <li><a href="#">Prospective Students</a></li>
                <li><a href="#">Current Students</a></li>
                <li><a href="#">Faculty & Staff</a></li>
                <li><a href="#">Alumni</a></li>
            </ul>
        </div>
    </div><!-- /.secondary-navigation -->
    <div class="primary-navigation-wrapper">
<?php include("pages/home_header.php");?>
    </div><!-- /.primary-navigation -->
    <div class="background">
        <!-- <img src="assets/img/background-city.png"  alt="background"> -->
    </div>
</div>
<!-- end Header -->

<!-- Page Content -->
<?php //include ('pages/home_content.php');
include_once ($content);
?>
<!-- end Page Content -->

<!-- Footer -->
<footer id="page-footer">
    <section id="footer-top">
        <div class="container">
            <div class="footer-inner">
                <div class="footer-social">
                    <figure>Follow us:</figure>
                    <div class="icons">
                        <a href=""><i class="fa fa-twitter"></i></a>
                        <a href=""><i class="fa fa-facebook"></i></a>
                        <a href=""><i class="fa fa-pinterest"></i></a>
                        <a href=""><i class="fa fa-youtube-play"></i></a>
                    </div><!-- /.icons -->
                </div><!-- /.social -->
                <div class="search pull-right">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                    <span class="input-group-btn">
                        <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                    </span>
                    </div><!-- /input-group -->
                </div><!-- /.pull-right -->
            </div><!-- /.footer-inner -->
        </div><!-- /.container -->
    </section><!-- /#footer-top -->

    <section id="footer-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <aside class="logo">
                        <img src="<?php echo WEB_ROOT;?>templates/info/assets/img/logo-white.png" class="vertical-center">
                    </aside>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-4">
                    <aside>
                        <header><h4>Contact Us</h4></header>
                        <address>
                            <strong>University of Universo</strong>
                            <br>
                            <span>4877 Spruce Drive</span>
                            <br><br>
                            <span>West Newton, PA 15089</span>
                            <br>
                            <abbr title="Telephone">Telephone:</abbr> +1 (734) 123-4567
                            <br>
                            <abbr title="Email">Email:</abbr> <a href="#">questions@youruniversity.com</a>
                        </address>
                    </aside>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-4">
                    <aside>
                        <header><h4>Important Links</h4></header>
                        <ul class="list-links">
                            <?php quick_links();?>
                        </ul>

                    </aside>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 col-sm-4">
                    <aside>
                        <header><h4>About Universo</h4></header>
                        <p>Aliquam feugiat turpis quis felis adipiscing, non pulvinar odio lacinia.
                            Aliquam elementum pharetra fringilla. Duis blandit, sapien in semper vehicula,
                            tellus elit gravida odio, ac tincidunt nisl mi at ante. Vivamus tincidunt nunc nibh.
                        </p>
                        <div>
                            <a href="" class="read-more">All News</a>
                        </div>
                    </aside>
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
        <div class="background"><img src="assets/img/background-city.png" class="" alt=""></div>
    </section><!-- /#footer-content -->

    <section id="footer-bottom">
        <div class="container">
            <div class="footer-inner">
                <div class="copyright">© College of Informatics, All rights reserved</div><!-- /.copyright -->
            </div><!-- /.footer-inner -->
        </div><!-- /.container -->
    </section><!-- /#footer-bottom -->

</footer>
<!-- end Footer -->

</div>
<!-- end Wrapper -->

<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/info/assets/js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/info/assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/info/assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/info/assets/js/selectize.min.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/info/assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/info/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/info/assets/js/jquery.placeholder.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/info/assets/js/jQuery.equalHeights.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/info/assets/js/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/info/assets/js/jquery.vanillabox-0.1.5.min.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/info/assets/js/retina-1.1.0.min.js"></script>

<script type="text/javascript" src="<?php echo WEB_ROOT;?>templates/info/assets/js/custom.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo WEB_ROOT;?>templates/cive/js/jquery.dataTables.js"></script>
<script src="<?php echo WEB_ROOT;?>templates/cive/js/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable( {
            "bFilter":   false,
            "dom": '<"top">rt<"bottom"p><"clear">'
        } );
    } );
</script>

<script>
    $(document).ready(function() {
        $('#dataTables-full').dataTable( {

        } );
    } );
</script>

</body>
</html>