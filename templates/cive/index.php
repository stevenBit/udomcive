<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>University of Dodoma &middot; College of Informatics</title>
  
    <link href="<?php echo WEB_ROOT;?>templates/cive/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo WEB_ROOT;?>templates/cive/bower_components/frankenstyle/frankenstyle.css" rel="stylesheet">
    <link href="<?php echo WEB_ROOT;?>templates/cive/bower_components/fontawesome/css/fontawesome.min.css" rel="stylesheet">
    <link href="<?php echo WEB_ROOT;?>templates/cive/libs/dropdown-menu/dropdown-menu.css" rel="stylesheet" type="text/css">
    <link href="<?php echo WEB_ROOT;?>templates/cive/libs/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css">
    <link href="<?php echo WEB_ROOT;?>templates/cive/libs/audioplayer/audioplayer.css" rel="stylesheet" type="text/css">
    <link href="<?php echo WEB_ROOT;?>templates/cive/css/main.css" rel="stylesheet">
    <link href="<?php echo WEB_ROOT;?>templates/cive/css/style.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link href="<?php echo WEB_ROOT;?>templates/cive/css/dataTables.bootstrap.css" rel="stylesheet">
    
          	<script type="text/javascript" src="<?php echo WEB_ROOT;?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo WEB_ROOT;?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo WEB_ROOT;?>assets/js/scripts.js"></script>
    
  </head>
  
  <body role="document">
	  <span class="visible-xs"></span>
	  <span class="visible-sm"></span>
	  <span class="visible-md"></span>
	  <span class="visible-lg"></span>
    
    <div id="k-head" class="container-fluid">
    	<div class="row">
            <nav class="k-functional-navig">
                <ul class="list-inline pull-right">
                    <li><a href="webmin/login.php" target='_blank'>Staff Login</a></li>
                </ul>
            </nav>
        	<div class="col-lg-12 kichwa">
        
        	  <div id="k-site-logo" class="pull-left">
                    <h1 class="k-logo">
                        <a href="index.php" title="Home Page">
                            <img src="<?php echo WEB_ROOT;?>templates/cive/img/civelogo2.jpg" alt="Site Logo" class="img-responsive" />
                        </a>
                    </h1>
                    
                    <a id="mobile-nav-switch" href="#drop-down-left">
		      <span class="alter-menu-icon"></span></a><!-- alternative menu button -->
            
            	</div><!-- site logo end -->
		

<!-- main navig
            	
		<nav id="k-menu" class="k-main-navig">
        
                    <ul id="drop-down-left" class="k-dropdown-menu">
                        <li>
                            <a href="news.html" title="Our School News">News</a>
                        </li>
                        <li>
                            <a href="events.html" title="Upcoming Events">Events</a>
                        </li>
                        <li><a href="courses.html" title="Available Courses">Courses</a></li>
                        <li><a href="about-us.html" title="How things work">About Us</a></li>
                        <li><a href="contact-us.html" title="School Contacts">Contact Us</a></li>
                    </ul>
        
            	</nav>
		-->
		<!-- main navig end -->
		
		
		 <?php
//urls
$soi="index.php?q=school_profile&s=1";
$sove="index.php?q=school_profile&s=2";
$non_degree="index.php?q=degree&d=Certificate";
$Diploma="index.php?q=degree&d=Diploma";
$degree="index.php?q=degree&d=Bachelor";
$master="index.php?q=degree&d=Masters";
$blog="index.php?q=blog";
$facility="index.php?q=facility&d=";
$alumn="index.php?q=alumn&d=";
$about="index.php?q=about";
$campuslife="index.php?q=campuslife&d=";
$fyp="index.php?q=student_projects";
			
?>
			
	  <!-- Toggle get grouped for better mobile display -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="fa fa-bars"></span>
            </button>
            <!-- End Toggle -->
			 <div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
			<nav class="k-main-navig navbar-responsive-collapse" role="navigation " id="k-menu" >
				<div class="navbar-header">
					 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span>
					 <span class="icon-bar"></span></button>
					 <a class="navbar-brand" href="#"></a>
				</div>
				
				
				
				<div class="collapse navbar-collapse">
					<ul class="k-dropdown-menu" id="drop-down-left">
						<li >
							<a href="index.php" title="">Home</a>
						</li>
						
						
						<li class="dropdown">
							 <a href="about.php" class="dropdown-toggle" data-toggle="dropdown" title="">
							About Us
							  <strong class="caret">
							    
							  </strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="<?php echo $about; ?>">About Us</a>
								</li>
								<li>
									<a href="<?php echo $facility; ?>">Facilities</a>
								</li>
								<li>
									<a href="<?php echo $campuslife; ?>">Campus Life</a>
								</li>
								
								<li>
									<a href="<?php echo $alumn; ?>">Our Students</a>
								</li>
								<li class="divider">
								</li>

							</ul>
						</li>
						
						
						
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="">
							 Academics
							  <strong class="caret">
							    
							  </strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="<?php echo $soi; ?>">School of Informatics</a>
								</li>
								<li>
									<a href="<?php echo $sove; ?>">School of Virtual Education</a>
								</li>
								</li>
								<li class="divider">
								</li>

							</ul>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="">
							  Admission
							  <strong class="caret">
							    
							  </strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="<?php echo $non_degree; ?>">Certificate</a>
								</li>
								<li>
									<a href="<?php echo $Diploma; ?>">Diploma</a>
								</li>
								<li>
									<a href="<?php echo $degree; ?>">Bachelor degreee</a>
								</li>
								
								<li>
									<a href="<?php echo $master; ?>">Postgraduate</a>
								</li>

								<li class="divider">
								</li>

							</ul>
						</li>
						
						
					      <li class="dropdown">
						    <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="">
							  Research
						    <strong class="caret"></strong>
						    </a>
							<ul class="dropdown-menu">
								<li>
									<a href="index.php?q=research&d=ResearchSection">Overview</a>
								</li>
								
								<li>
									<a href="index.php?q=publication">Publications</a>
								</li>
								<li class="divider"></li>

							</ul>
						</li>
					      
					      <li>
							<a href="<?php echo $blog; ?>" title="">Blog</a>
					      </li>
						
						
					</ul>
				</div>
				
			</nav>

		</div>
		
            
            </div>
            
        </div><!-- row end -->
    
    </div><!-- container + head wrapper end -->
    
<div id="k-body"><!-- content wrapper -->
    
        	<?php
		
                include_once $content;
		
                ?> 
    
</div><!-- content wrapper end -->
    
    <div id="k-footer"><!-- footer -->
    
    	<div class="container"><!-- container -->
        
        	<div class="row no-gutter"><!-- row -->
            
            	<div class="col-lg-4 col-md-4"><!-- widgets column left -->
            
                    <div class="col-padded col-naked">
                    
                        <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                        	<li class="widget-container widget_nav_menu"><!-- widgets list -->
                    
                                <h1 class="title-widget">Useful links</h1>
                                
<?php quick_links();?>
                    
                    
							</li>
                            
                        </ul>
                         
                    </div>
                    
                </div><!-- widgets column left end -->
                
                <div class="col-lg-4 col-md-4"><!-- widgets column center -->
                
                    <div class="col-padded col-naked">
                    
                        <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                        	<li class="widget-container widget_recent_news"><!-- widgets list -->
                    
                                <h1 class="title-widget">Our Contact</h1>
                                
                                <div itemscope itemtype="http://data-vocabulary.org/Organization"> 
                                
                                	<h2 class="title-median m-contact-subject" itemprop="name">College of Informatics</h2>
                                
                                	<div class="m-contact-address" itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
                                		<span class="m-contact-city-region">
											<span class="m-contact-zip-country">
												<span class="m-contact-zip" itemprop="postal-code">P.O.Box 490</span> <br> 
												<span class="m-contact-city" itemprop="locality">Dodoma</span>, 
												<span class="m-contact-country" itemprop="country-name">Tanzania</span>
											</span>
										</span>
                                	</div>
                                     
                                	<div class="m-contact-tel-fax">
                                    	<span class="m-contact-tel">Tel: <span itemprop="tel">+255-26-2310118</span></span>
                                    	<span class="m-contact-fax">Fax: <span itemprop="fax">+255-26-2310118</span></span>
                                    </div>
                                    
                                </div>
                                
                                <div class="social-icons">
                                
                                	<ul class="list-unstyled list-inline">
                                    
                                    	<li><a href="#" title="Contact us"><i class="fa fa-envelope"></i></a></li>
                                        <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                    
                                    </ul>
                                
                                </div>
                    
							</li>
                            
                        </ul>
                        
                    </div>
                    
                </div><!-- widgets column center end -->
                
                <div class="col-lg-4 col-md-4"><!-- widgets column right -->
                
                    <div class="col-padded col-naked">
                    
                        <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                        	<li class="widget-container widget_sofa_flickr"><!-- widgets list -->
                    
                                <h1 class="title-widget">VIRTUAL TOUR</h1>
                                
<?php
campus_tour ();
?>
                    
							</li>
                            
                        </ul> 
                        
                    </div>
                
                </div><!-- widgets column right end -->
            
            </div><!-- row end -->
        
        </div><!-- container end -->
    
    </div><!-- footer end -->
    
    <div id="k-subfooter"><!-- subfooter -->
    
    	<div class="container"><!-- container -->
        
        	<div class="row"><!-- row -->
            
            	<div class="col-lg-12">
                
                	<p class="copy-text text-inverse">
                    &copy; 2015 University of Dodoma &middot; College of Informatics. All rights reserved.
                    </p>
                
                </div>
            
            </div><!-- row end -->
        
        </div><!-- container end -->
    
    </div><!-- subfooter end -->

    <!-- jQuery -->
      <script src="<?php echo WEB_ROOT;?>templates/cive/bower_components/jquery/dist/jquery.min.js"></script>
<!--
    <script src="jQuery/jquery-migrate-1.2.1.min.js"></script>
-->
    
    <!-- Bootstrap -->
    <script src="<?php echo WEB_ROOT;?>templates/cive/bower_components/bootstrap/js/bootstrap.min.js"></script>
    
    <!-- Drop-down -->
    <script src="<?php echo WEB_ROOT;?>templates/cive/libs/dropdown-menu/dropdown-menu.js"></script>
    
    <!-- Fancybox -->
	<script src="<?php echo WEB_ROOT;?>templates/cive/libs/fancybox/jquery.fancybox.pack.js"></script>
    <script src="<?php echo WEB_ROOT;?>templates/cive/libs/fancybox/jquery.fancybox-media.js"></script><!-- Fancybox media -->
    
    <!-- Responsive videos -->
    <script src="<?php echo WEB_ROOT;?>templates/cive/libs/jquery.fitvids.js"></script>
    
    <!-- Audio player -->
	<script src="<?php echo WEB_ROOT;?>templates/cive/libs/audioplayer/audioplayer.min.js"></script>
    
    <!-- Pie charts -->
    <script src="<?php echo WEB_ROOT;?>templates/cive/libs/jquery.easy-pie-chart.js"></script>
    
    <!-- Theme -->
    <script src="<?php echo WEB_ROOT;?>templates/cive/libs/theme.js"></script>
    
    
<!-- DataTables JavaScript -->
    <script src="<?php echo WEB_ROOT;?>templates/cive/js/jquery.dataTables.js"></script>
    <script src="<?php echo WEB_ROOT;?>templates/cive/js/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function() {
    $('#dataTables-example').dataTable( {
        "bFilter":   false,
	"dom": '<"top">rt<"bottom"p><"clear">'
    } );
} );
	</script>

<script>
    $(document).ready(function() {
    $('#dataTables-full').dataTable( {
       
    } );
} );
	</script>


    

    
    
    
  </body>
</html>
