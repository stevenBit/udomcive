-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 21, 2015 at 11:13 AM
-- Server version: 5.1.44
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `udomcive`
--

-- --------------------------------------------------------

--
-- Table structure for table `pec_mssgs`
--

CREATE TABLE IF NOT EXISTS `pec_mssgs` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `uid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `m` tinyint(2) NOT NULL DEFAULT '0',
  `d` tinyint(2) NOT NULL DEFAULT '0',
  `y` smallint(4) NOT NULL DEFAULT '0',
  `start_time` time NOT NULL DEFAULT '00:00:00',
  `end_time` time NOT NULL DEFAULT '00:00:00',
  `title` varchar(50) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `m` (`m`),
  KEY `y` (`y`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pec_mssgs`
--

INSERT INTO `pec_mssgs` (`id`, `uid`, `m`, `d`, `y`, `start_time`, `end_time`, `title`, `text`) VALUES
(1, 1, 4, 12, 2015, '55:55:55', '55:55:55', 'staff evaluation', 'kukagua taarifa za satff');

-- --------------------------------------------------------

--
-- Table structure for table `pec_users`
--

CREATE TABLE IF NOT EXISTS `pec_users` (
  `uid` smallint(6) NOT NULL AUTO_INCREMENT,
  `username` char(15) NOT NULL DEFAULT '',
  `password` char(32) NOT NULL DEFAULT '',
  `fname` char(20) NOT NULL DEFAULT '',
  `lname` char(30) NOT NULL DEFAULT '0',
  `userlevel` tinyint(2) NOT NULL DEFAULT '0',
  `email` char(40) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pec_users`
--

INSERT INTO `pec_users` (`uid`, `username`, `password`, `fname`, `lname`, `userlevel`, `email`) VALUES
(1, 'admin', 'password', 'default', 'user', 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_academic_profile`
--

CREATE TABLE IF NOT EXISTS `tbl_academic_profile` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Level` varchar(255) NOT NULL,
  `Institution` varchar(255) NOT NULL,
  `GraduationDate` varchar(255) NOT NULL,
  `StaffID` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_academic_profile`
--

INSERT INTO `tbl_academic_profile` (`Id`, `Name`, `Level`, `Institution`, `GraduationDate`, `StaffID`) VALUES
(3, 'Computer Science', 'Bachelor of Science', 'University of Dar es Salaam', '2009', 1),
(2, 'Computer Science', 'Master of Science', 'Malmo University', '2012', 1),
(4, 'Computer Engineering', 'Bachelor of Science', 'University of Dar es Salaam', '2013', 1),
(10, 'Computer Science', 'Master of Science', 'University of Dar es Salaam', '2012', 78),
(9, 'Telecomunication', 'Master of Science', 'University of Dodoma', '2011', 91),
(11, 'Computer Engineering', 'Master of Science', 'University of Dar es Salaam', '2012', 65);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_academic_year`
--

CREATE TABLE IF NOT EXISTS `tbl_academic_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `academic` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `semester` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_academic_year`
--

INSERT INTO `tbl_academic_year` (`id`, `academic`, `description`, `semester`, `status`) VALUES
(1, '2006/2007', '', 1, 0),
(2, '2007/2008', '', 1, 0),
(3, '2008/2009', '', 1, 0),
(4, '2009/2010', '', 1, 0),
(5, '2010/2011', '', 1, 0),
(6, '2011/2012', '', 1, 0),
(7, '2012/2013', '', 1, 0),
(8, '2013/2014', '', 1, 0),
(9, '2014/2015', '', 1, 1),
(10, '2015/2016', '', 1, 0),
(11, '2016/2017', '', 1, 0),
(12, '2017/2018', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_alumn`
--

CREATE TABLE IF NOT EXISTS `tbl_alumn` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Biography` text NOT NULL,
  `Programme` varchar(500) NOT NULL,
  `Image` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `PostDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_alumn`
--

INSERT INTO `tbl_alumn` (`Id`, `Name`, `Biography`, `Programme`, `Image`, `PostDate`) VALUES
(1, 'Juma Raphaelsss', '<p>Juma is a graduate of the Masters Programme in Computer Science. Why did you decide to come to CIVE ? Before coming to Sweden, I completed a bachelorï¿½s degree in software engineering at Iran University of Science and Technology and then got some work experience in my field. Thereafter, I felt I needed to deepen my academic knowledge, so I started looking for opportunities to continue my studies. I did some research online to find out more about the different study options, and I discovered Sweden to be a nice place to live and study. Further, I found Malmï¿½ University a great place for pursuing my academic career. I was really inspired by the innovative approach of the Masterï¿½s Programme in Computer Science here. How do you like Sweden so far? I didnï¿½t know a lot about Sweden before I came here. I had heard that Sweden is a peaceful and calm country to live in, which, of course, was appealing. Now that I have lived here for over a year, I have also started to appreciate the Swedish nature. Sweden is not very densely populated, so there is a lot of wild nature to explore. Last summer, we had an opportunity to travel around Sweden and saw some wonderful places. I thought that Sweden would be a very cold country; but as Malmï¿½ is in southern Sweden, even my hometown in Iran would be much colder at times. Malmï¿½ is a really great place to live and to study because it has a very multi-cultural environment; in addition, its geographical location is great, with lots of things to do nearby.</p>', 'Bachelor of Science in Computer Science', '2015-HT-5626.jpg', '2015-01-01 21:33:50'),
(2, 'Moses Juma', '<p>Naipenda UDOM</p>', 'Bachelor of Science in Software Engineering', '2015-HT-7703.jpg', '2015-05-20 20:25:51');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_article`
--

CREATE TABLE IF NOT EXISTS `tbl_article` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Category` varchar(255) NOT NULL,
  `Access` int(11) NOT NULL DEFAULT '1',
  `Title` varchar(255) NOT NULL,
  `Content` text NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  `PostDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Image` varchar(255) NOT NULL DEFAULT 'article.jpg',
  `File1` varchar(255) NOT NULL,
  `File2` varchar(255) NOT NULL,
  `File3` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `tbl_article`
--

INSERT INTO `tbl_article` (`Id`, `Category`, `Access`, `Title`, `Content`, `Status`, `PostDate`, `Image`, `File1`, `File2`, `File3`) VALUES
(47, '5', 1, '<p>Visit of the Cooperate Vice President of Microsoft Cooperation on 5th Dec 2013.</p>', '<p>ON 05th December, the Cooperate Vice President Dr.&nbsp;David Tennenhouse&nbsp;and other delegates of&nbsp;&nbsp;Microsoft Corporation pay a visit at the College of Informatics and Virtual Education.&nbsp;The main purpose of a visit was to visit the Microsoft Innovation Center (MIC) of the University of Dodoma and held a discussion on TV White Space Project, this is a collaborative<br />project between COSTECH, UhuruOne and Microsoft which is demonstrating<br />Private&nbsp; and Public Partnership.</p>\r\n<p>Microsoft delegates were accompanied by representatives from COSTECH, Mr. Faith Shimba and Mihayo Wilmore from UhuruOne and hosted by the MIC under its coordinator Mr Matogoro Jabhera.&nbsp;</p>\r\n<p>Kindly have a look on the Photos of the management meeting held at CIVE Principals'' OffIce chaired by the Ag. Principal Prof</p>', 1, '2015-04-11 00:18:40', '2015-HT-2439.jpg', '', '', ''),
(50, '8', 1, '<p>About CIVE</p>', '<p>Located in Dodoma, the Capital City of Tanzania, College of Informatics and Virtual Education of the University of Dodoma started in 2007 with School of Informatics as an interim arrangement for setting up the College. It became a Campus College of the University in July 2008 with two Schools; School of Informatics (SoI) and School of Virtual Education (SoVE). SoI is responsible for delivering Information and Communication Technology (ICT) programs at all levels. SoVE is mandated to facilitate delivery of University programs using ICT technology in a virtual environment and to build capacity in virtual training, learning and delivery. The College has established a Centre for Innovation, Research and Development in ICT to coordinate and spearhead research and innovation. It has also set up a Consultancy and Professional Services Bureau as its outreach services wing.</p>', 1, '2015-05-21 12:39:18', '', '', '', ''),
(51, '9', 1, '<p>Research Section</p>', '<p>Research Section</p>', 1, '2015-05-21 12:50:51', '', '', '', ''),
(43, '2', 1, '<p>Digital Health in Action - Fighting Counterfeit Drugs with mHealth Technology</p>', '<p>Digital Health is a broad term which encompasses overlapping technology sectors across healthcareï¿½as evidenced by our Venn diagram representing the Digital Health Landscape. In this ï¿½Digital Health in Actionï¿½ series, weï¿½ll focus on specific examples from each domain to show what an impact the digital health movement is making on healthcare across the world. Here, we take a look at how mhealth technology is being used to fight against counterfeit drugs in the marketplace. Counterfeit medications are any drugs which are not what they purport to be, and mhealth technology is being used on the Digital Health Landscape to decrease their flow. They could be medications without active ingredients or with incorrect quantities of active ingredients. They may contain wrong ingredients or, in some cases, high levels of impurities and contaminants. They may be copies of other original products or may come with fake packaging. The High Cost of Fake Drugs The full scope of the problem of counterfeit medicines is difficult to investigate due to underreporting in many countries and because of inherent difficulties in detecting, investigating and quantifying counterfeiting. But what is certain is that the delivery of genuine and quality health services is severely undermined by fake medicines. A 2003 factsheet by WHO quotes the estimates of the US Food and Drug Administration that, ï¿½counterfeits make up more than 10% of the global medicines market and are present in both industrialized and developing countries. It is estimated that up to 25% of the medicines consumed in poor countries are counterfeit or substandard.ï¿½ The World Customs Organization calculates that the global fake drugs market could be as large as $200 billion each year. Counterfeiting is Killing People In 2009, the International Policy Network reported that fake anti-malarial and anti-tuberculosis drugs alone cause nearly 700,000 deaths each year in developing countries. The report also states that 46 per cent of drugs sold in Angola, Congo and Burundi are substandard, 68 per cent of anti-malarial drugs sold in Laos, Myanmar, Cambodia and Vietnam do not contain sufficient quantities of active ingredients, and most of these fake drugs originate from China and India. The dangers from fake drugs are threefold: They fail to treat a condition because of no or insufficient active ingredients. They cause direct harm due to the presence of impurities and contaminants. They promote drug resistance in harmful microorganisms due to their overall inefficiency. Some of the important factors hindering the fight against fake drugs include: Lack of political will Absence of appropriate drug legislation Poor regulation and law enforcement Corruption High prices of medicines High profit margins in counterfeiting Insufficient coordination among stakeholders in the healthcare community Inadequate global coordination mHealth Technology Can Help Combat Counterfeiting The ubiquitous presence of mobile phones, even in low and middle income countries, provides an opportunity to enable consumers to check the authenticity of medicines using SMS text messaging services. The advantage of this method is that it bypasses bureaucratic lethargy in regulatory authorities and poor law enforcement situations prevalent in many countries. The manufacturers and distributors of genuine medicines also have a strong incentive to adopt this technologyï¿½to prevent potential loss of business due to fake drugs and to preserve their brand reputations. This SMS-based product verification method relies on printing a unique one-time identification codeï¿½usually a series of numbersï¿½on product packs and then concealing them. When a purchaser scratches off the concealing panel at point-of-purchase and texts the revealed code to a central registry, a message is sent almost instantly specifying whether the product is authentic or suspicious. Unlike the previous methods used by the pharmaceutical industryï¿½such as trademarked brands, tamper-proof packaging, and holographic imagesï¿½this end-to-end SMS system is believed to be more effective. Printing millions of unique codes using mass serialization systems, itï¿½s complex enough to make any counterfeiting efforts by fake drug syndicates less profitable. The first SMS-based anti-counterfeit program was launched by mPedigree in Ghana in 2007. Sproxil, a Cambridge, Massachusetts-based company, also launched a similar system in the United States in 2009 and has since expanded its operations in Ghana, Nigeria, Kenya and India. Sproxilï¿½s award-winning Mobile Product Authentication (MPA) is the most widely used SMS-based anti-counterfeit system. So far, the company has processed more than 11 million verifications. This is a very impressive achievement because the number of times consumers choose to verify codes is a better indicator of the technologyï¿½s social impact than the number of unique codes printed on medications. SMS-Based Drug Verification is Scalable and Sustainable The critical success factors behind Sproxilï¿½s business model need to be understood so as to replicate and geographically expand similar usage of mobile technology to fight counterfeit medication. Sproxil engages economic buyersï¿½pharmaceutical manufacturers and distributorsï¿½who suffer most from counterfeiting efforts, and also benefit most from countering them. Supply chain visibility and recovered sales revenue from the reduction of fake medicines in the marketplace are strong incentives for these players to adopt this technology. Sproxil also increases the net utility for these buyers by aggregating across many drug manufacturers, which makes the costs of implementing SMS-based verification cheaper because of economies of scale. By selling the same technology to other industriesï¿½such as automotive, agribusiness and FMCGï¿½Sproxil further strengthened the financial sustainability of the business model. Using mobile technology for product verification bypasses government and bureaucratic layers and lessens the need for complex partnerships in the healthcare ecosystem. Sproxil, however, carefully engages governments, too. All countries where it operates have endorsed the service because of an increased awareness of the problems posed by fake drugs. Mobile network operatorsï¿½another important stakeholder in the modelï¿½also have an incentive of being able to earn more revenues from SMS services without having to do anything outside their core competencies. Sproxil is also diversifying from anti-counterfeiting by moving towards providing market intelligence and analytics services based on the data obtained from the use of its service. All of these factors have enabled SMS-based verification to overcome scalability and sustainability issuesï¿½two common obstacles in leveraging the full potential of many promising mHealth technologies. Robust expansion of SMS-based verification technology would create a positive impact on health outcomes by decreasing fake drugs, improving communication between various stakeholders in the drug supply chain, and reducing stock-outs of essential medicines.</p>', 1, '2015-04-07 14:03:25', 'madawa.jpg', '', '', ''),
(44, '4', 1, '<p>Digital Health in Action- Reducing Stock-Outs of Essential Medications Using SMS Messages</p>', '<p>Digital Health is a broad term which encompasses overlapping technology sectors across healthcareï¿½as evidenced by our Venn diagram representing the Digital Health Landscape. In this ï¿½Digital Health in Actionï¿½ series, weï¿½ll focus on specific examples from each domain to show what an impact the digital health movement is making on healthcare across the world. Using mobile communication devices to deliver health-related services is increasingly becoming an inseparable component of the global healthcare ecosystem. On the Digital Health Landscape, applications of mHealth in prevention, diagnosis, treatment and patient monitoring are well known, but its utility extends further into other layers of the healthcare community, such as supply chain management. Enabling access to quality healthcare for all people in a society necessitates the facilitation of free flow and optimal use of health-related goods and systems. Supply chain constraints faced in delivering medical supplies to needed locations in a timely manner can adversely affect the ability of healthcare systems to function effectively. Stock-out of essential medicines, especially in rural and remote areas, creates a serious bottleneck with tangible impacts on health outcomes. In many developing countries, where patients get free medicines at government-run rural health centers, stock-outs often force them to go to private clinics and pharmaciesï¿½adding undo financial burden. The lack of timely information regarding stock levels of essential medicines impacts the ability of health management authorities to make critical decisions regarding the maintenance of medication supplies, and the ability to forecast the need for drugs and handle emergency orders. SMS Messages Making a Difference ï¿½SMS for Lifeï¿½ is a public-private partnership program that seeks to counter these problems using mobile technology. Using SMS messaging between district health management authorities and point-of-care health facilities that distribute medicines, the visibility of medicine stock levels can be improvedï¿½thereby reducing stock-outs. Part of the ï¿½Roll Back Malariaï¿½ program, SMS for Life was first launched in 2009 in three districts of Tanzania, covering 229 villages and a population of 1.2 million people. Using SMS messages, mobile telephones, and electronic mapping technology, the initiative facilitated a comprehensive and accurate stock count of all anti-malarial drugs in 129 health facilities. The district health management authority typically sends a stock-request message to all registered health facility workers. They then count the anti-malarial drug stocks and send the information back to the system using SMS messages. The district health authorities use the received information to electronically map the availability of drugs and orders distribution of drugs to the sites and/or re-distribution between the sites to improve the stock levels. ï¿½SMS for Lifeï¿½ Getting Results During the pilot study period of 21 weeks, the stock count data was provided in 95% cases with 94% accuracy. The proportion of health facilities with no stock of anti-malarial drugs fell from 78% at week 1 to 26% at week 21. In one district, stock-outs were eliminated completely by week 8. At the beginning of the project, 26% of the point-of-care facilities had no dose form of ACT, an anti-malarial drug, and by the end this figure had been cut to less than 1%. Stock-out issues that previously used to take about one to two months to resolve can now be completed in less than two days. ï¿½SMS for Lifeï¿½ Expanding Its Impact Currently, SMS for Life has been expanded to more than 5,100 health facilities in Tanzania. In Ghana, a full-scale country-wide system is being put in place to track the stock availability of 28 blood products, after a successful pilot project in six districts. In Kenya, following the completion of an extensive pilot project, a full country scale-up is underway with the help of the National Malaria Control Program (NMCP). A similar scale-up operation is in progress in Cameroon, covering 3,800 health facilities in 91 districts. SMS for Life is also presently being implemented in Congo covering 1,245 health facilities. By using these types of mhealth applications in countries around the world, solutions are being achieved to meet supply chain challengesï¿½enabling improved access to quality healthcare for communities across the globe.</p>', 1, '2015-04-07 14:03:25', 'sms.jpg', '', '', ''),
(46, '2', 1, '<p>How Big Data Is Helping BIOFEM To Fight Counterfeit Drugs</p>', '<p>Every year in Africa, 100,000 deaths are linked to the counterfeit drug trade, according to the World Health Organization (WHO). As Africaï¿½s largest drugs market with over 70% of its drugs imported from India and China, considered the ï¿½biggest source of fakesï¿½, news headlines have been awash with stories of how counterfeit drugs are causing a menace in Nigeria for years. For example, in one 1989 incident, over 150 children died as a result of paracetamol syrup containing diethylene glycol. In 2008, over 80 children died after taking cough syrup contaminated with antifreeze. Still, in 2011, 64% of Nigeriaï¿½s imported antimalarial drugs were said to be fake, according to the World Health Organization (WHO). In fact, the problem of fake drugs became so severe that neighbouring countries such as Ghana and Sierra Leone officially banned the sale of drugs, foods and beverages products made in Nigeria. But aside that it cost lives, counterfeit drugs are costing pharmaceutical companies huge revenues. Indeed, the United Nations estimates the yearly trade in counterfeit drugs at more than $500 billion worldwide, with West African nations like Nigeria a key hot spot for dealers. Time and time again, the countryï¿½s government agency responsible for regulating and controlling the manufacture, importation, exportation, advertisement, distribution, sale and use of food, drugs, cosmetics, medical devices, chemicals and packaged water has taken bold steps to checkmate illicit and counterfeit drugs, but the issue has never abated. In a country where resources are already scarce, it became clear that fighting fake drugs required what weï¿½ve termed ï¿½the MAD approachï¿½ ï¿½ an efficient and easily distributed solution that is powered by Mobile, Analytics and Data. Thatï¿½s why when Merck, a global pharmaceutical and chemical company based in Darmstadt, Germany had first-hand experience with the challenges of drug counterfeiting in Nigeria through its exclusive distributor and one of Nigeriaï¿½s largest, BIOFEM Pharmaceuticals Ltd, which imports and distributes over 20 name brand and generic drugs, it took ï¿½the MAD approachï¿½ to restoring the integrity of its supply chain and removing the risks to the health of its customers, as well as its brand. An unidentified woman tests mobile startup Sproxil''s drug authentication service on her cellphone in this handout photo. REUTERS/handout/Sproxil How Big Data solved the problem Prior to 2008, BIOFEM had been experiencing continuous growth for Glucophage, an important diabetes drug and one of its top-selling Merck products in Nigeria. But just a year later, it had witnessed a 75 percent reduction in Glucophage revenues, which the company and Merck attributed almost entirely to the siphoning effect of counterfeit Glucophage sales. By leveraging large pools of data brought together and analyzed to discern patterns and make better decisions, the company sought the help of a drug authenticity tracking solution (known as Mobile Product Authentication, or MPA) developed by a U.S-based start-up named Sproxil, Inc. Sproxilï¿½s solution provided pharmaceutical manufacturers with the means to extract actionable business intelligence from the body of transaction data it collects. It further helped consumers avoid counterfeit drug purchases by allowing them send a free text message using a unique code off the label from their mobile phone to an SMS short code provided on the package directly from their point-of-sale. Upon receiving the code, the solution checks it against the MPAï¿½s authentication database and logs the transaction for future analysis. If the package is found to be authentic, the customer receives an ï¿½OKï¿½ notification on his mobile phone, along with relevant product data and a phone number to call for additional information. If not, the customer receives a warning notification. In addition, the resulting data stream can be viewed and analyzed using an IBM data visualization software to provide a top-down view on counterfeit selling patterns, thereby making it easier to spot patterns for specific products in specific countries. The solution increased revenues, saved millions of lives The solution gathered consumer-sourced purchasing data that was used to substantially improve anti-counterfeiting efforts. The pilot phase which ran for 100 days started off with the diabetes drug, Glucophage 500mg, and 735,000 packs were initially distributed with scratch stickers carrying code affixed to the blister pack which could be sent via SMS to a toll free number. The 100-day trial ran in 3 cities, namely, Lagos, Abuja and Port Harcourt, and the Sproxil system handled more than 22,600 inbound and outbound SMS messages over the duration of the pilot phase from 6761 unique users. To date, there have been over 13 million successful verifications of products processed through the solution. As a result of adopting ï¿½the MAD approachï¿½, BIOFEM experienced a 10 percent increase in revenues for its top-selling drug an a 1,000% Return on Investment (ROI) based on incremental revenue gains within three months, and the long slide in BIOFEMï¿½s market share due to sales of fake Glucophage abruptly halted. The simplicity of the solution enabled us to implement it on over 1 million packages, representing 25 million customers, in less than three months. This rapid response improved our customersï¿½ safety and helped us prevent lost sales.ï¿½ ï¿½ Femi Soremekun, Managing Director at Biofem Pharmaceuticals Ltd. Apart from that, the solution changed the way pharmacists opted to source the drug, Glucophage, preferring to sell only labeled, guaranteed authentic products, since they now had knowledge that customers had become better educated about the value of buying anti-counterfeit labeled products. With thousands of deaths every year attesting to the fact that itï¿½s always impossible to discern a counterfeit drug from a legitimate one, this BIOFEM case study has clearly shown that Big Data and Analytics solutions when deployed can help pharmaceutical manufacturers create significant value to consumers by reducing cost and waste, increase the quality of products and build a basis for competitive advantage in Africa.</p>', 1, '2015-04-07 18:49:38', 'verify.jpg', '', '', ''),
(48, '3', 1, '<p>Campus life article</p>', '<p>Cive is the place to be</p>', 1, '2015-04-19 23:45:48', '2015-HT-4152.jpg', '', '', ''),
(49, '7', 1, '<p>Principal Message</p>', '<p>Welcome to the College of Informatics and Virtual Education of the University of Dodoma. Strategically located in Dodoma city, the centre and the national capital of Tanzania, the college is destined to be a centre of excellence in ICT. Our mission will always be to provide comprehensive, gender sensitive and quality education to a broad segment of the population through teaching, research and public services in ICT. Blessed with competent, dedicated, self-motivated and highly skilled academic, technical and administrative staff, the college has been able to make remarkable achievements despite our young history. Since its establishment in 2008, the College has produced three batches of graduates with Certificate, Diploma and Degree qualifications in various specialties. Our graduates</p>', 1, '2015-05-21 00:09:26', '2015-HT-0466.jpg', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_article_category`
--

CREATE TABLE IF NOT EXISTS `tbl_article_category` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_article_category`
--

INSERT INTO `tbl_article_category` (`Id`, `Name`, `Status`) VALUES
(1, 'Mobile ', 1),
(2, 'News', 1),
(3, 'Campus Life', 1),
(4, 'Facility', 1),
(5, 'Announcement', 1),
(6, 'Documents', 1),
(7, 'PrincipalMessage', 1),
(8, 'AboutCollege', 1),
(9, 'ResearchSection', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_article_comment`
--

CREATE TABLE IF NOT EXISTS `tbl_article_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `PostDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_article_comment`
--

INSERT INTO `tbl_article_comment` (`id`, `article_id`, `comment`, `email`, `author`, `status`, `PostDate`) VALUES
(1, 39, 'safi sana', 'deoshayo@gmail.com', 'Deo', 0, '2015-04-06 02:31:09'),
(2, 39, 'imetulia sana', 'deoshayo@gmail.com', 'Grace', 0, '2015-04-06 02:31:54'),
(3, 40, 'imekaa vema', 'deoshayo@gmail.com', 'Steve', 0, '2015-04-06 02:39:38'),
(4, 46, 'Safi sana', 'deoshayo@gmail.com', 'DEO', 0, '2015-04-08 15:04:23'),
(5, 46, 'Safi sana', 'deoshayo@gmail.com', 'DEO', 0, '2015-04-08 15:04:32'),
(6, 44, 'SGAIIII', 'deoshayo@gmail.com', 'Steve', 0, '2015-04-08 18:03:28'),
(7, 46, 'safiiiiiii sanaanana', 'deoshayo@gmail.com', 'joseph', 0, '2015-04-17 00:40:44');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blocked_ip`
--

CREATE TABLE IF NOT EXISTS `tbl_blocked_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(300) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_blocked_ip`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_college`
--

CREATE TABLE IF NOT EXISTS `tbl_college` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Principal` int(11) NOT NULL,
  `Biography` text NOT NULL,
  `Principal_Message` text NOT NULL,
  `Research_Biography` text NOT NULL,
  `PostDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_college`
--

INSERT INTO `tbl_college` (`Id`, `Name`, `Description`, `Address`, `Principal`, `Biography`, `Principal_Message`, `Research_Biography`, `PostDate`) VALUES
(1, 'CIVE', 'cive', 'P.O.BOX 490 DODOMA', 1, 'Located in Dodoma, the Capital City of Tanzania, College of Informatics and Virtual Education of the University of Dodoma started in 2007 with School of Informatics as an interim arrangement for setting up the College. It became a Campus College of the University in July 2008 with two Schools; School of Informatics (SoI) and School of Virtual Education (SoVE). SoI is responsible for delivering Information and Communication Technology (ICT) programs at all levels. SoVE is mandated to facilitate delivery of University programs using ICT technology in a virtual environment and to build capacity in virtual training, learning and delivery. The College has established a Centre for Innovation, Research and Development in ICT to coordinate and spearhead research and innovation. It has also set up a Consultancy and Professional Services Bureau as its outreach services wing.', 'Welcome to the College of Informatics and Virtual Education of the University of Dodoma. Strategically located in Dodoma city, the centre and the national capital of Tanzania, the college is destined to be a centre of excellence in ICT.  Our mission will always be to provide comprehensive, gender sensitive and quality education to a broad segment of the population through teaching, research and public services in ICT.\r\n \r\nBlessed with competent, dedicated, self-motivated and highly skilled academic, technical and administrative staff, the college has been able to make remarkable achievements despite our young history. Since its establishment in 2008, the College has produced three batches of graduates with Certificate, Diploma and Degree qualifications in various specialties. Our graduates have demonstrated exceptional strengths and qualities that have amazed many. They have secured reputable jobs in the Central Government, Local Governments, Parastatal Organizations, Non-Governmental Organizations and Private Institutions within and outside the country. Some of our graduates have been bold enough to start their own companies. The College will continually strive to produce qualified and competitive graduates in specialized ICT areas.\r\n \r\nAs we start new academic year, we expect each member of the college community to be innovative in all his/her endeavor and to originate new ideas which are vital ingredients in any live and vibrant academic institution.\r\n \r\nI wish you all a prosperous new academic year 2013-2014.', 'Our research focuses is on ICT4D', '2014-12-09 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cordinator`
--

CREATE TABLE IF NOT EXISTS `tbl_cordinator` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `StudyLevel` int(11) NOT NULL,
  `StaffID` int(11) DEFAULT NULL,
  `Description` varchar(255) NOT NULL,
  `Biography` text NOT NULL,
  `EntryRequirement` text NOT NULL,
  `Application` text NOT NULL,
  `Fee_structure` text NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Name` (`Name`),
  KEY `StudyLevel` (`StudyLevel`),
  KEY `StaffID` (`StaffID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_cordinator`
--

INSERT INTO `tbl_cordinator` (`Id`, `Name`, `StudyLevel`, `StaffID`, `Description`, `Biography`, `EntryRequirement`, `Application`, `Fee_structure`, `Status`) VALUES
(1, 'Certificate', 1, 65, '<p>Certificate&nbsp;&nbsp;Programmes</p>', '<p>we offere various coures</p>', '<p>Entry Points</p>', '<p>Apply via TCU</p>', '<p>pay pay</p>', 1),
(2, 'Bachelor', 3, 65, '<p>Degree Programmes</p>', '<p>Ou courses equip students with skills to excel</p>', '<p>Entry Points</p>', '<p>Apply via TCU</p>', '<p>YOU PAY BEFORE CLASS</p>', 1),
(3, 'Masters', 4, 65, '<p>Posgraduate Programmes</p>', '<p>we offer these masters courses</p>', '<p>Entry Points</p>', '<p>Apply via TCU</p>', '<p>pay first</p>', 1),
(6, 'Diploma', 2, 65, '<p>Diploma</p>', '<p>Diploma</p>', '<p>Diploma</p>', '<p>Diploma</p>', '<p>Diploma</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course`
--

CREATE TABLE IF NOT EXISTS `tbl_course` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `Content` varchar(500) NOT NULL,
  `Unit` float NOT NULL DEFAULT '10',
  `DepartmentID` int(11) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  `studyLevelId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=113 ;

--
-- Dumping data for table `tbl_course`
--

INSERT INTO `tbl_course` (`Id`, `Name`, `Description`, `Content`, `Unit`, `DepartmentID`, `Status`, `studyLevelId`) VALUES
(1, 'IS 110', 'Introduction To Information Systems', '', 10, 0, 1, 1),
(2, 'CS 110', 'Introduction To High Level Programming', '', 10, 0, 1, 1),
(3, 'DS 103', 'Development Perspectives', '', 10, 0, 1, 1),
(4, 'LG 103', 'Business Communication', '', 10, 0, 1, 1),
(5, 'TN 110', 'Introduction To Discrete Mathematics', '', 10, 0, 1, 1),
(6, 'CN 110', 'Fundamentals Of Engineering Drawing With CAD', '', 10, 0, 1, 1),
(7, 'TN 111', 'Linear Algebra', '', 10, 0, 1, 1),
(8, 'CS 111', 'Principles Of Security', '', 10, 0, 1, 1),
(9, 'IS 111', 'Medical And Surgical Conditions', '', 10, 0, 1, 1),
(10, 'TN 112', 'Fundamentals Of Electrical Engineering', '', 10, 0, 1, 1),
(11, 'TN 113', 'Introduction To Telecommunication Engineering', '', 10, 0, 1, 1),
(12, 'BT 112', 'Introduction To Information Technology', '', 10, 0, 1, 1),
(13, 'TN 114', 'Business Mathematics', '', 7, 0, 1, 1),
(14, 'CD 110', 'Multimedia Technology I', '', 7, 0, 1, 1),
(15, 'CD 111', 'Introduction To Educational Technology', '', 7, 0, 1, 1),
(16, 'FB 110', 'Introduction To Virtual Education', '', 7, 0, 1, 1),
(17, 'CS 213', 'Object Oriented Programming In Java', '', 10, 0, 1, 1),
(18, 'TN 211', 'Computer Networking Protocols', '', 10, 0, 1, 1),
(19, 'CS 211', 'Systems Analysis And Design', '', 10, 0, 1, 1),
(20, 'CS 212', 'Software Development I', '', 10, 0, 1, 1),
(21, 'CN 210', 'Computer Architecture And Organization', '', 10, 0, 1, 1),
(22, 'CS 214', 'Linux/Unix Systems Administration And Management', '', 10, 0, 1, 1),
(23, 'TN 213', 'Analogue Electronics I', '', 7, 0, 1, 1),
(24, 'BT 211', 'Business Law And Ethics', '', 10, 0, 1, 1),
(25, 'BT 212', 'E-Business Strategy, Architecture And Design', '', 10, 0, 1, 1),
(26, 'BT 210', 'Computerized Accounting Application', '', 10, 0, 1, 1),
(27, 'IS 210', 'Information System Strategy', '', 7, 0, 1, 1),
(28, 'CS 215', 'Information Security Technologies', '', 10, 0, 1, 1),
(29, 'IS 211', 'Health Information Systems I', '', 10, 0, 1, 1),
(30, 'IS 213', 'Health Law And Ethics', '', 10, 0, 1, 1),
(31, 'IS 212', 'Information Systems Management', '', 10, 0, 1, 1),
(32, 'TN 216', 'Measurement And Instrumentation Engineering', '', 10, 0, 1, 1),
(33, 'TN 212', 'Electrical Network Analysis', '', 7, 0, 1, 1),
(34, 'TN 210', 'Calculus II', '', 10, 0, 1, 1),
(35, 'TN 214', 'Deterministic Signals And Systems', '', 7, 0, 1, 1),
(36, 'TN 215', 'Electromagnetic Theory', '', 7, 0, 1, 1),
(37, 'CD 214', 'Script Writing And Storyboarding', '', 10, 0, 1, 1),
(38, 'CD 216', 'Introduction To Web Design', '', 7, 0, 1, 1),
(39, 'CD 213', 'Fundamentals Of 2D And 3D Animation ', '', 10, 0, 1, 1),
(40, 'CD 215', 'Foundations Of Instructional Design', '', 10, 0, 1, 1),
(41, 'SS 342 ', 'Project Appraisal, Analysis And Evaluation', '', 0, 0, 1, 1),
(42, 'BT 400 ', 'Professional Ethics And Conduct', '', 0, 0, 1, 1),
(43, 'CS 322 ', 'Object Oriented Program Design And Analysis', '', 0, 0, 1, 1),
(44, 'CS 306 ', 'Internet Programming And Applications', '', 0, 0, 1, 1),
(45, 'IS 303 ', 'Information And Communication Systems Security', '', 0, 0, 1, 1),
(46, 'TN 305 ', 'Digital Electronics II', '', 0, 0, 1, 1),
(47, 'CN 301 ', 'Computer Hardware And Maintenance', '', 0, 0, 1, 1),
(48, 'TN 300 ', 'Wireless And Mobile Networking', '', 0, 0, 1, 1),
(49, 'CS 300 ', 'Distributed Database Systems', '', 0, 0, 1, 1),
(50, 'BT 330 ', 'Selected Topics In Business Information Systems', '', 0, 0, 1, 1),
(51, 'CS 340 ', 'Software Project Management', '', 0, 0, 1, 1),
(52, 'CS 341 ', 'Database Management System', '', 0, 0, 1, 1),
(53, 'TN 309 ', 'Microwave Communication', '', 0, 0, 1, 1),
(54, 'TN 330 ', 'Selected Topics In Telecom. Engineering', '', 0, 0, 1, 1),
(55, 'TN 310 ', 'LAN Switching', '', 0, 0, 1, 1),
(56, 'CS 312 ', 'Software Design And Implementation', '', 0, 0, 1, 1),
(57, 'CS 314 ', 'Software Testing And Quality Assurance', '', 0, 0, 1, 1),
(58, 'IS 304 ', 'Geographic Information Systems', '', 0, 0, 1, 1),
(59, 'MT 201 ', 'Numerical Analysis', '', 0, 0, 1, 1),
(60, 'CS 330 ', 'Operating System Security', '', 0, 0, 1, 1),
(61, 'CS 331', 'Information Security Management', '', 0, 0, 1, 1),
(62, 'DM 277', 'Health Law And Ethics', '', 0, 0, 1, 1),
(63, 'DM 377', 'Leadership And Management In Health', '', 0, 0, 1, 1),
(64, 'IS 307', 'E-Health', '', 0, 0, 1, 1),
(65, 'CS 304', 'Human Computer Interaction', '', 0, 0, 1, 1),
(66, 'TN 307', 'Analog Electronics II', '', 0, 0, 1, 1),
(67, 'CD 313', 'Realistic Concept Of Animation And Special Effects', '', 0, 0, 1, 1),
(68, 'CD 311', 'Audio & Video Technology', '', 0, 0, 1, 1),
(69, 'CD 310', 'Digital Photography And Technology', '', 0, 0, 1, 1),
(70, 'CD 314', '3D Modelling And Animation', '', 0, 0, 1, 1),
(71, 'CD 301', 'Program Evaluation In Virtual Education', '', 0, 0, 1, 1),
(72, 'CD 304', 'International Issues In Open And Virtual Learning', '', 0, 0, 1, 1),
(73, 'CN 401 ', 'Parallel Computing ', '', 0, 0, 1, 1),
(74, 'TN 400', 'WAN And MAN Technologies And Design', '', 0, 0, 1, 1),
(75, 'CS 316', 'Digital Image Processing', '', 0, 0, 1, 1),
(76, 'CS 401', 'Software Maintenance', '', 0, 0, 1, 1),
(77, 'TN 405', 'Digital Signal Processing', '', 0, 0, 1, 1),
(78, 'TN 406', 'Analogue Filters', '', 0, 0, 1, 1),
(79, 'TN 401', 'Wireless And Mobile Communication', '', 0, 0, 1, 1),
(80, 'BT 0010', 'Fundamentals Of Information Technology', '', 0, 0, 1, 1),
(81, 'CS 0010', 'Computing Mathematics I', '', 0, 0, 1, 1),
(82, 'BT 0011', 'Bussiness Communications', '', 0, 0, 1, 1),
(83, 'TN 0010', 'Introduction To Internet Architecture And Protocols ', '', 0, 0, 1, 1),
(84, 'TN 0011', 'Introduction To Analogue Electronics', '', 0, 0, 1, 1),
(85, 'TN 0012', 'Introduction To Electromagnetics I', '', 0, 0, 1, 1),
(86, 'TN 0013', 'Introduction To Electromagnetic Measurement And Instrumentation', '', 0, 0, 1, 1),
(87, 'BT 0013', 'Basic Computer Applications', '', 0, 0, 1, 1),
(88, 'CS 0011', 'Introduction To Operating Systems', '', 0, 0, 1, 1),
(89, 'CS 0012', 'Software Management', '', 0, 0, 1, 1),
(90, 'FB 0010', 'Introduction To Educational Technology', '', 0, 0, 1, 1),
(91, 'CD 0011', 'Fundamentals Of Audio And Video Technologies', '', 0, 0, 1, 1),
(92, 'CS 0015 ', 'Web Fundamentals', '', 0, 0, 1, 1),
(93, 'CS 0110', 'Computing Mathematics', '', 0, 0, 1, 1),
(94, 'CS 0111', 'Introduction To IT Security', '', 0, 0, 1, 1),
(95, 'BT 0115', 'Introduction To Information Technology', '', 0, 0, 1, 1),
(96, 'CD 0119', 'Multimedia Technologies', '', 0, 0, 1, 1),
(97, 'CD 0114', 'Desktop Publishing', '', 0, 0, 1, 1),
(98, 'FB 0115', 'E-learning Tools', '', 0, 0, 1, 1),
(99, 'CN 0111', 'Fundamentals Of Computer Hardware', '', 0, 0, 1, 1),
(100, 'TN 0111', 'Basics Of Electrical And Electronics Engineering', '', 0, 0, 1, 1),
(101, 'LG 0103', 'Communication Skills', '', 0, 0, 1, 1),
(102, 'FB 0111', 'Introduction To Educational Technologies', '', 0, 0, 1, 1),
(103, 'FB 0114', 'Introduction To Instructional Design', '', 0, 0, 1, 1),
(104, 'FB 0110', 'Teaching Methodology', '', 0, 0, 1, 1),
(105, 'FB 0112', 'Introduction To Educational Psychology', '', 0, 0, 1, 1),
(106, 'FB 0113', 'Curriculum Development And Teaching', '', 0, 0, 1, 1),
(107, 'CS 0210', 'Design And Implementation Of Object Oriented Applications', '', 0, 0, 1, 1),
(108, 'CS 0211', 'Introduction To Database Systems', '', 0, 0, 1, 1),
(109, 'CS 0212', 'Basic Calculus', '', 0, 0, 1, 1),
(110, 'CS 0213', 'Network And Systems Administration', '', 0, 0, 1, 1),
(111, 'CS 0214', 'Introduction To Computer Graphics', '', 0, 0, 1, 1),
(112, 'CS 0215', 'Electronic Commerce', '', 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course_selection`
--

CREATE TABLE IF NOT EXISTS `tbl_course_selection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseID` int(11) NOT NULL,
  `academicID` int(11) NOT NULL,
  `staffID` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:chief;2:Assist',
  PRIMARY KEY (`id`),
  KEY `courseID` (`courseID`),
  KEY `academicID` (`academicID`),
  KEY `staffID` (`staffID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `tbl_course_selection`
--

INSERT INTO `tbl_course_selection` (`id`, `courseID`, `academicID`, `staffID`, `status`) VALUES
(69, 80, 9, 65, 1),
(71, 82, 9, 126, 1),
(72, 83, 9, 126, 1),
(81, 87, 9, 65, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_curiculum`
--

CREATE TABLE IF NOT EXISTS `tbl_curiculum` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProgramID` int(11) NOT NULL,
  `CourseID` int(11) NOT NULL,
  `YearOfStudy` varchar(255) NOT NULL,
  `Semister` varchar(255) NOT NULL,
  `Core` int(11) NOT NULL DEFAULT '1',
  `Status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `ProgramID` (`ProgramID`),
  KEY `CourseID` (`CourseID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=280 ;

--
-- Dumping data for table `tbl_curiculum`
--

INSERT INTO `tbl_curiculum` (`Id`, `ProgramID`, `CourseID`, `YearOfStudy`, `Semister`, `Core`, `Status`) VALUES
(1, 9, 1, '1', '1', 1, 1),
(2, 12, 1, '1', '1', 1, 1),
(3, 3, 1, '1', '1', 1, 1),
(4, 1, 1, '1', '1', 1, 1),
(5, 11, 1, '1', '1', 1, 1),
(6, 14, 1, '1', '1', 1, 1),
(7, 15, 1, '1', '1', 1, 1),
(8, 9, 2, '1', '1', 1, 1),
(9, 12, 2, '1', '1', 1, 1),
(10, 3, 2, '1', '1', 1, 1),
(11, 1, 2, '1', '1', 1, 1),
(12, 11, 2, '1', '1', 1, 1),
(13, 14, 2, '1', '1', 1, 1),
(14, 15, 2, '1', '1', 1, 1),
(15, 10, 2, '1', '1', 1, 1),
(16, 5, 2, '1', '1', 1, 1),
(17, 13, 2, '1', '1', 1, 1),
(18, 9, 3, '1', '1', 1, 1),
(19, 12, 3, '1', '1', 1, 1),
(20, 3, 3, '1', '1', 1, 1),
(21, 1, 3, '1', '1', 1, 1),
(22, 11, 3, '1', '1', 1, 1),
(23, 14, 3, '1', '1', 1, 1),
(24, 15, 3, '1', '1', 1, 1),
(25, 10, 3, '1', '1', 1, 1),
(26, 5, 3, '1', '1', 1, 1),
(27, 13, 3, '1', '1', 1, 1),
(28, 9, 4, '1', '1', 1, 1),
(29, 12, 4, '1', '1', 1, 1),
(30, 3, 4, '1', '1', 1, 1),
(31, 1, 4, '1', '1', 1, 1),
(32, 11, 4, '1', '1', 1, 1),
(33, 14, 4, '1', '1', 1, 1),
(34, 15, 4, '1', '1', 1, 1),
(35, 10, 4, '1', '1', 1, 1),
(36, 5, 4, '1', '1', 1, 1),
(37, 13, 4, '1', '1', 1, 1),
(38, 9, 5, '1', '1', 1, 1),
(39, 12, 5, '1', '1', 1, 1),
(40, 1, 5, '1', '1', 1, 1),
(41, 11, 5, '1', '1', 1, 1),
(42, 14, 5, '1', '1', 1, 1),
(43, 15, 5, '1', '1', 1, 1),
(44, 10, 6, '1', '1', 1, 1),
(45, 13, 6, '1', '1', 1, 1),
(46, 10, 7, '1', '1', 1, 1),
(47, 3, 7, '1', '1', 1, 1),
(48, 1, 7, '1', '1', 1, 1),
(49, 5, 7, '1', '1', 1, 1),
(50, 13, 7, '1', '1', 1, 1),
(51, 3, 8, '1', '1', 1, 1),
(52, 11, 9, '1', '1', 1, 1),
(53, 10, 10, '1', '1', 1, 1),
(54, 13, 10, '1', '1', 1, 1),
(55, 5, 10, '1', '1', 1, 1),
(56, 10, 11, '1', '1', 1, 1),
(57, 13, 11, '1', '1', 1, 1),
(58, 9, 12, '1', '1', 1, 1),
(59, 12, 12, '1', '1', 1, 1),
(60, 3, 12, '1', '1', 1, 1),
(61, 1, 12, '1', '1', 1, 1),
(62, 11, 12, '1', '1', 1, 1),
(63, 14, 12, '1', '1', 1, 1),
(64, 15, 12, '1', '1', 1, 1),
(65, 10, 12, '1', '1', 1, 1),
(66, 5, 12, '1', '1', 1, 1),
(67, 13, 12, '1', '1', 1, 1),
(68, 9, 13, '1', '1', 1, 1),
(69, 9, 14, '1', '1', 1, 1),
(70, 14, 14, '1', '1', 1, 1),
(71, 15, 14, '1', '1', 1, 1),
(72, 9, 15, '1', '1', 1, 1),
(73, 15, 15, '1', '1', 1, 1),
(74, 9, 16, '1', '1', 1, 1),
(75, 15, 16, '1', '1', 1, 1),
(76, 9, 17, '2', '1', 1, 1),
(77, 3, 17, '2', '1', 1, 1),
(78, 1, 17, '2', '1', 1, 1),
(79, 11, 17, '2', '1', 1, 1),
(80, 14, 17, '2', '1', 1, 1),
(81, 5, 17, '2', '1', 1, 1),
(82, 12, 17, '2', '1', 1, 1),
(83, 10, 17, '2', '1', 1, 1),
(84, 13, 17, '2', '1', 1, 1),
(85, 9, 18, '2', '1', 2, 1),
(86, 10, 18, '2', '1', 1, 1),
(87, 3, 18, '2', '1', 1, 1),
(88, 1, 18, '2', '1', 1, 1),
(89, 11, 18, '2', '1', 2, 1),
(90, 12, 18, '2', '1', 1, 1),
(91, 5, 18, '2', '1', 1, 1),
(92, 13, 18, '2', '1', 1, 1),
(93, 14, 18, '2', '1', 1, 1),
(94, 9, 19, '2', '1', 1, 1),
(95, 10, 19, '2', '1', 1, 1),
(96, 3, 19, '2', '1', 1, 1),
(97, 1, 19, '2', '1', 1, 1),
(98, 11, 19, '2', '1', 1, 1),
(99, 12, 19, '2', '1', 1, 1),
(100, 5, 19, '2', '1', 1, 1),
(101, 13, 19, '2', '1', 1, 1),
(102, 14, 19, '2', '1', 1, 1),
(103, 3, 20, '2', '1', 2, 1),
(104, 1, 20, '2', '1', 1, 1),
(105, 5, 20, '2', '1', 1, 1),
(106, 14, 20, '2', '1', 2, 1),
(107, 3, 21, '2', '1', 1, 1),
(108, 1, 21, '2', '1', 1, 1),
(109, 11, 21, '2', '1', 2, 1),
(110, 12, 21, '2', '1', 1, 1),
(111, 5, 21, '2', '1', 1, 1),
(112, 9, 22, '2', '1', 1, 1),
(113, 3, 22, '2', '1', 1, 1),
(114, 1, 22, '2', '1', 1, 1),
(115, 11, 22, '2', '1', 1, 1),
(116, 12, 22, '2', '1', 2, 1),
(117, 5, 22, '2', '1', 1, 1),
(118, 10, 23, '2', '1', 1, 1),
(119, 1, 23, '2', '1', 2, 1),
(120, 13, 23, '2', '1', 1, 1),
(121, 9, 24, '2', '1', 1, 1),
(122, 9, 25, '2', '1', 1, 1),
(123, 12, 25, '2', '1', 1, 1),
(124, 9, 26, '2', '1', 1, 1),
(125, 12, 26, '2', '1', 2, 1),
(126, 9, 27, '2', '1', 2, 1),
(127, 11, 27, '2', '1', 1, 1),
(128, 12, 27, '2', '1', 1, 1),
(129, 3, 28, '2', '1', 1, 1),
(130, 12, 28, '2', '1', 2, 1),
(131, 11, 29, '2', '1', 1, 1),
(132, 11, 30, '2', '1', 1, 1),
(133, 11, 31, '2', '1', 2, 1),
(134, 12, 31, '2', '1', 1, 1),
(135, 10, 32, '2', '1', 1, 1),
(136, 13, 32, '2', '1', 1, 1),
(137, 10, 33, '2', '1', 1, 1),
(138, 13, 33, '2', '1', 1, 1),
(139, 10, 34, '2', '1', 1, 1),
(140, 13, 34, '2', '1', 1, 1),
(141, 13, 35, '2', '1', 1, 1),
(142, 10, 36, '2', '1', 1, 1),
(143, 13, 36, '2', '1', 1, 1),
(144, 14, 37, '1', '1', 1, 1),
(145, 14, 38, '2', '1', 1, 1),
(146, 14, 39, '2', '1', 1, 1),
(147, 14, 40, '2', '1', 2, 1),
(148, 9, 41, '3', '1', 1, 1),
(149, 12, 41, '3', '1', 1, 1),
(150, 9, 42, '3', '1', 1, 1),
(151, 3, 42, '3', '1', 1, 1),
(152, 11, 42, '3', '1', 1, 1),
(153, 12, 42, '3', '1', 1, 1),
(154, 9, 43, '3', '1', 1, 1),
(155, 10, 43, '3', '1', 1, 1),
(156, 12, 43, '3', '1', 1, 1),
(157, 5, 43, '3', '1', 2, 1),
(158, 13, 43, '3', '1', 1, 1),
(159, 9, 44, '3', '1', 1, 1),
(160, 3, 44, '3', '1', 1, 1),
(161, 1, 44, '3', '1', 2, 1),
(162, 12, 44, '3', '1', 1, 1),
(163, 9, 45, '3', '1', 1, 1),
(164, 10, 45, '3', '1', 1, 1),
(165, 3, 45, '3', '1', 1, 1),
(166, 1, 45, '3', '1', 1, 1),
(167, 11, 45, '3', '1', 1, 1),
(168, 12, 45, '3', '1', 1, 1),
(169, 5, 45, '3', '1', 1, 1),
(170, 13, 45, '3', '1', 1, 1),
(171, 15, 45, '3', '1', 1, 1),
(172, 10, 46, '3', '1', 1, 1),
(173, 3, 46, '3', '1', 2, 1),
(174, 13, 46, '3', '1', 1, 1),
(175, 10, 47, '3', '1', 2, 1),
(176, 13, 47, '3', '1', 1, 1),
(177, 10, 48, '3', '1', 1, 1),
(178, 11, 48, '3', '1', 2, 1),
(179, 13, 48, '3', '1', 1, 1),
(180, 10, 49, '3', '1', 1, 1),
(181, 5, 49, '3', '1', 1, 1),
(182, 14, 49, '3', '1', 1, 1),
(183, 9, 50, '3', '1', 2, 1),
(184, 11, 50, '3', '1', 2, 1),
(185, 12, 50, '3', '1', 2, 1),
(186, 9, 51, '3', '1', 2, 1),
(187, 10, 51, '3', '1', 2, 1),
(188, 13, 53, '3', '1', 1, 1),
(189, 13, 54, '3', '1', 2, 1),
(190, 13, 55, '3', '1', 2, 1),
(191, 5, 56, '3', '1', 1, 1),
(192, 15, 56, '3', '1', 1, 1),
(193, 5, 57, '3', '1', 1, 1),
(194, 11, 58, '3', '1', 1, 1),
(195, 12, 58, '3', '1', 2, 1),
(196, 3, 59, '3', '1', 2, 1),
(197, 1, 59, '3', '1', 1, 1),
(198, 3, 60, '3', '1', 1, 1),
(199, 3, 61, '3', '1', 1, 1),
(200, 11, 62, '3', '1', 1, 1),
(201, 11, 63, '3', '1', 1, 1),
(202, 11, 64, '3', '1', 1, 1),
(203, 11, 65, '3', '1', 1, 1),
(204, 11, 66, '3', '1', 1, 1),
(205, 13, 66, '3', '1', 1, 1),
(206, 14, 67, '3', '1', 1, 1),
(207, 14, 68, '3', '1', 1, 1),
(208, 14, 69, '3', '1', 1, 1),
(209, 14, 70, '3', '1', 1, 1),
(210, 15, 71, '3', '1', 1, 1),
(211, 15, 72, '3', '1', 2, 1),
(212, 5, 55, '4', '1', 2, 1),
(213, 13, 55, '4', '1', 2, 1),
(214, 10, 73, '4', '1', 1, 1),
(215, 1, 73, '3', '1', 2, 1),
(216, 10, 42, '4', '1', 1, 1),
(217, 5, 42, '4', '1', 1, 1),
(218, 13, 42, '4', '1', 1, 1),
(219, 10, 74, '4', '1', 1, 1),
(220, 13, 74, '4', '1', 1, 1),
(221, 10, 75, '4', '1', 2, 1),
(222, 5, 65, '4', '1', 1, 1),
(223, 5, 76, '4', '1', 1, 1),
(224, 10, 41, '4', '1', 1, 1),
(225, 5, 41, '4', '1', 1, 1),
(226, 13, 41, '4', '1', 1, 1),
(227, 5, 77, '4', '1', 2, 1),
(228, 13, 77, '4', '1', 2, 1),
(229, 13, 78, '4', '1', 2, 1),
(230, 22, 80, '1', '1', 1, 1),
(231, 6, 80, '1', '1', 1, 1),
(232, 20, 80, '1', '1', 1, 1),
(233, 22, 81, '1', '1', 1, 1),
(234, 6, 81, '1', '1', 1, 1),
(235, 20, 81, '1', '1', 1, 1),
(236, 22, 82, '1', '1', 1, 1),
(237, 6, 82, '1', '1', 1, 1),
(238, 20, 82, '1', '1', 1, 1),
(239, 6, 83, '1', '1', 1, 1),
(240, 6, 84, '1', '1', 1, 1),
(241, 6, 85, '1', '1', 1, 1),
(242, 6, 86, '1', '1', 1, 1),
(243, 20, 87, '1', '1', 1, 1),
(244, 22, 88, '1', '1', 1, 1),
(245, 20, 88, '1', '1', 1, 1),
(246, 20, 89, '1', '1', 1, 1),
(247, 22, 90, '1', '1', 1, 1),
(248, 22, 91, '1', '1', 1, 1),
(249, 22, 92, '1', '1', 1, 1),
(250, 20, 92, '1', '1', 1, 1),
(251, 16, 93, '1', '1', 1, 1),
(252, 17, 93, '1', '1', 1, 1),
(253, 18, 93, '1', '1', 1, 1),
(254, 16, 94, '1', '1', 1, 1),
(255, 17, 94, '1', '1', 1, 1),
(256, 18, 94, '1', '1', 1, 1),
(257, 16, 95, '1', '1', 1, 1),
(258, 17, 95, '1', '1', 1, 1),
(259, 18, 95, '1', '1', 1, 1),
(260, 16, 96, '1', '1', 1, 1),
(261, 17, 96, '1', '1', 1, 1),
(262, 18, 96, '1', '1', 1, 1),
(263, 17, 97, '1', '1', 1, 1),
(264, 16, 98, '1', '1', 1, 1),
(265, 17, 98, '1', '1', 1, 1),
(266, 16, 99, '1', '1', 1, 1),
(267, 16, 101, '1', '1', 1, 1),
(268, 17, 101, '1', '1', 1, 1),
(269, 18, 101, '1', '1', 1, 1),
(270, 18, 102, '1', '1', 1, 1),
(271, 18, 104, '1', '1', 1, 1),
(272, 18, 105, '1', '1', 1, 1),
(273, 18, 106, '1', '1', 1, 1),
(274, 19, 107, '2', '1', 1, 1),
(275, 19, 108, '2', '1', 1, 1),
(276, 19, 109, '2', '1', 1, 1),
(277, 19, 110, '2', '1', 1, 1),
(278, 19, 111, '2', '1', 1, 1),
(279, 19, 112, '2', '1', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_database_autobackup`
--

CREATE TABLE IF NOT EXISTS `tbl_database_autobackup` (
  `backupId` int(11) NOT NULL AUTO_INCREMENT,
  `backupDate` date NOT NULL,
  `dbName` varchar(200) NOT NULL,
  `backuptype` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`backupId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_database_autobackup`
--

INSERT INTO `tbl_database_autobackup` (`backupId`, `backupDate`, `dbName`, `backuptype`) VALUES
(1, '2014-09-01', 'db-backup-1409598452-f880db14734e7e8fda30e248f4ff2ec6.sql', 0),
(7, '2014-08-06', 'db-backup-1407342611-246af4782d0677dffa4b9a42c694a43c.sql', 0),
(8, '2014-09-01', 'db-backup-1409598540-f880db14734e7e8fda30e248f4ff2ec6.sql', 0),
(9, '2014-09-16', 'db-backup-1410844305-f4cb3110d65acfe5f48e7330e8fa5464.sql', 0),
(10, '2014-10-01', 'db-backup-1412140361-f4cb3110d65acfe5f48e7330e8fa5464.sql', 0),
(11, '2014-10-16', 'db-backup-1413439464-f09757c6b81939abd3d485aca6020a10.sql', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_database_backup`
--

CREATE TABLE IF NOT EXISTS `tbl_database_backup` (
  `backupId` int(11) NOT NULL AUTO_INCREMENT,
  `backupDate` date NOT NULL,
  `dbName` varchar(200) NOT NULL,
  `backuptype` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`backupId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tbl_database_backup`
--

INSERT INTO `tbl_database_backup` (`backupId`, `backupDate`, `dbName`, `backuptype`) VALUES
(5, '2014-10-09', 'db-backup-1412892964-f09757c6b81939abd3d485aca6020a10.sql', 0),
(6, '2014-10-09', 'db-backup-1412894638-f09757c6b81939abd3d485aca6020a10.sql', 0),
(7, '2014-10-09', 'db-backup-1412894773-f09757c6b81939abd3d485aca6020a10.sql', 0),
(8, '2014-10-09', 'db-backup-1412894833-f09757c6b81939abd3d485aca6020a10.sql', 0),
(9, '2014-10-09', 'db-backup-1412894873-f09757c6b81939abd3d485aca6020a10.sql', 0),
(10, '2014-10-09', 'db-backup-1412894994-f09757c6b81939abd3d485aca6020a10.sql', 0),
(11, '2014-10-09', 'db-backup-1412895168-f09757c6b81939abd3d485aca6020a10.sql', 0),
(12, '2014-10-09', 'db-backup-1412899157-f09757c6b81939abd3d485aca6020a10.sql', 0),
(13, '2014-10-09', 'db-backup-1412899617-f09757c6b81939abd3d485aca6020a10.sql', 0),
(14, '2014-10-12', 'db-backup-1413141693-f09757c6b81939abd3d485aca6020a10.sql', 0),
(15, '2014-10-22', 'db-backup-1414004631-7fc9eaa75bb53835f006c4a1bf1e2318.sql', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department`
--

CREATE TABLE IF NOT EXISTS `tbl_department` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `HOD` int(11) NOT NULL,
  `SchoolID` int(11) NOT NULL,
  `Biography` text NOT NULL,
  `Email` varchar(255) NOT NULL,
  `MobileNo` varchar(255) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Name` (`Name`),
  KEY `SchoolID` (`SchoolID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_department`
--

INSERT INTO `tbl_department` (`Id`, `Name`, `Description`, `HOD`, `SchoolID`, `Biography`, `Email`, `MobileNo`, `Status`) VALUES
(1, 'Virtual Educational Technologies and Application', 'Virtual Educational Technologies and Application', 1, 1, 'Information and Communication Technology have the potentials of transforming governance by fostering transparency and accountability. Technologies such as Web 2.0, mobile technologies and interactive mapping have a capability of addressing public problems by uncovering corruption and create openness in the governance (David et al., 2010). Table 1 presents ICTs for citizen engagement. ', '', '', 1),
(2, 'Computer Science', '<p>Computer Sciences</p>', 65, 1, '<p>ICT BASED</p>', 'dvs.cive@gmail.com', '0758749476', 1),
(3, 'Telecommunication Engineering', '<p>Telecommunication Engineering</p>', 1, 2, '<p>Telecommunication Engineerings</p>', 'deoshayo@gmail.com', '0894746565', 1),
(5, 'Department of Information System', 'Department of Information System', 0, 1, 'Department of Information System', '', '', 1),
(6, 'Department of Computer Engineering and Application', '<p>Department of Computer Engineering and Application</p>', 126, 2, '<p>Department of Computer Engineering and Application</p>', '', '', 1),
(7, 'Department of Telecommunication and Computer Networks ', 'Department of Telecommunication and Computer Networks ', 0, 1, 'Department of Telecommunication and Computer Networks ', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_designation`
--

CREATE TABLE IF NOT EXISTS `tbl_designation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_designation`
--

INSERT INTO `tbl_designation` (`id`, `designation`, `description`) VALUES
(1, 'Mr.', ''),
(2, 'Mrs.', ''),
(3, 'Ms.', ''),
(4, 'Miss.', ''),
(5, 'Dr.', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_document`
--

CREATE TABLE IF NOT EXISTS `tbl_document` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) NOT NULL,
  `Category` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `Url` varchar(255) NOT NULL,
  `Format` varchar(255) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  `PostDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_document`
--

INSERT INTO `tbl_document` (`Id`, `Title`, `Category`, `Description`, `Url`, `Format`, `Status`, `PostDate`) VALUES
(3, 'Exam Results', 'Announcement', 'Matokeo ya mithani', 'www.udom.ac.tz', '0', 1, '0000-00-00 00:00:00'),
(1, 'Exam Results', 'Announcement', 'Matokeo ya mithani', 'www.udom.ac.tz', '0', 1, '0000-00-00 00:00:00'),
(2, 'udoma resoulution', 'Announcement', 'KIKAO KIKAO', 'www.udom.ac.tz', '0', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fyp`
--

CREATE TABLE IF NOT EXISTS `tbl_fyp` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Category` varchar(255) NOT NULL,
  `Access` int(11) NOT NULL DEFAULT '1',
  `Title` varchar(255) NOT NULL,
  `Content` text NOT NULL,
  `Members` text NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  `PostDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `File` varchar(255) NOT NULL DEFAULT 'fyp.pdf',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_fyp`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_fyp_group`
--

CREATE TABLE IF NOT EXISTS `tbl_fyp_group` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProjectID` int(11) NOT NULL,
  `StudentID` int(11) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `ProjectID` (`ProjectID`,`StudentID`),
  KEY `RegNo` (`StudentID`),
  KEY `ProjectID_2` (`ProjectID`),
  KEY `StudentID` (`StudentID`),
  KEY `ProjectID_3` (`ProjectID`),
  KEY `StudentID_2` (`StudentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `tbl_fyp_group`
--

INSERT INTO `tbl_fyp_group` (`Id`, `ProjectID`, `StudentID`, `Status`) VALUES
(35, 1, 37, 1),
(36, 1, 38, 1),
(37, 1, 39, 1),
(38, 3, 40, 1),
(39, 3, 41, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fyp_project`
--

CREATE TABLE IF NOT EXISTS `tbl_fyp_project` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) NOT NULL,
  `Abstract` text NOT NULL,
  `Supervisor` int(11) NOT NULL,
  `Report` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `Status` int(11) NOT NULL DEFAULT '1',
  `PostDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `Supervisor` (`Supervisor`),
  KEY `Supervisor_2` (`Supervisor`),
  KEY `Supervisor_3` (`Supervisor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_fyp_project`
--

INSERT INTO `tbl_fyp_project` (`Id`, `Title`, `Abstract`, `Supervisor`, `Report`, `Image`, `Status`, `PostDate`) VALUES
(1, 'Momath for primary', '<p>The File Record Management System (FRMS) contains features that facilitate management of files from various agencies archived at the National Record Center.&nbsp;</p>\r\n<p>The File Record Management System (FRMS) contains features that facilitate management of files from various agencies archived at the National Record Center.</p>\r\n<p>The File Record Management System (FRMS) contains features that facilitate management of files from various agencies archived at the National Record Center.</p>\r\n<p>v</p>\r\n<p>The File Record Management System (FRMS) contains features that facilitate management of files from various agencies archived at the National Record Center.</p>', 65, '', 'default.jpg', 1, '2015-04-27 02:46:37'),
(3, 'Development of CIVE website', '<p>Implementation of a cive community portal</p>', 65, '', 'default.jpg', 1, '2015-05-20 21:43:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fyp_student`
--

CREATE TABLE IF NOT EXISTS `tbl_fyp_student` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `RegNo` varchar(255) NOT NULL,
  `ProgramID` varchar(255) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `RegNo` (`RegNo`),
  KEY `ProgramID` (`ProgramID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `tbl_fyp_student`
--

INSERT INTO `tbl_fyp_student` (`Id`, `Name`, `RegNo`, `ProgramID`, `Status`) VALUES
(1, 'Shaban Issa', 'T/2010/2008', '1', 1),
(2, 'Omary Sadam', 'T/2010/2009', '2', 1),
(37, 'Jumanne Ilakoze', 'T/UDOM/2013/008', '1', 1),
(38, 'Sara Ernest', 'T/UDOM/2013/009', '3', 1),
(39, 'Musa Uledi', 'T/UDOM/2013/010', '1', 1),
(40, 'Rose Haji', 'T/UDOM/2013/0200', '1', 1),
(41, 'Musa Semkae', 'T/UDOM/2013/019', '3', 1),
(42, 'Hamis Juma', 'T/UDOM/2013/017', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_link`
--

CREATE TABLE IF NOT EXISTS `tbl_link` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Url` varchar(255) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  `Category` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_link`
--

INSERT INTO `tbl_link` (`Id`, `Name`, `Url`, `Status`, `Category`) VALUES
(3, 'Loan Board', 'http://www.heslb.go.tz', 1, 'Quick'),
(2, 'Main Administration', 'http://www.udom.ac.tz', 1, 'Quick'),
(4, 'Ministry of Education', 'http://www.moe.go.tz', 1, 'Quick'),
(5, 'Loan Board', 'http://www.heslb.go.tz', 1, 'Quick'),
(6, 'Ministry of Education', 'http://www.moe.go.tz', 1, 'Quick');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_loghistory`
--

CREATE TABLE IF NOT EXISTS `tbl_loghistory` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IpAddress` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `DateVisited` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `User_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `browser` varchar(300) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Page` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=143 ;

--
-- Dumping data for table `tbl_loghistory`
--

INSERT INTO `tbl_loghistory` (`Id`, `IpAddress`, `DateVisited`, `User_id`, `browser`, `Page`) VALUES
(1, '::1', '2015-05-20 22:05:03', '65', 'Google Chrome', 'login'),
(2, '::1', '2015-05-20 22:05:07', '65', 'Google Chrome', 'middle.php'),
(3, '::1', '2015-05-20 22:05:12', '65', 'Google Chrome', 'user_privilege.php'),
(4, '::1', '2015-05-20 22:05:14', '65', 'Google Chrome', 'user_privilege.php'),
(5, '::1', '2015-05-20 22:05:21', '65', 'Google Chrome', 'user_privilege.php'),
(6, '::1', '2015-05-20 22:05:31', '65', 'Google Chrome', 'user_privilege.php'),
(7, '::1', '2015-05-20 22:05:33', '65', 'Google Chrome', 'middle.php'),
(8, '::1', '2015-05-20 22:05:35', '65', 'Google Chrome', 'department_list.php'),
(9, '::1', '2015-05-20 22:05:39', '65', 'Google Chrome', 'department_list.php'),
(10, '::1', '2015-05-20 22:05:47', '65', 'Google Chrome', 'department_list.php'),
(11, '::1', '2015-05-20 22:05:50', '65', 'Google Chrome', 'department_list.php'),
(12, '::1', '2015-05-20 22:05:58', '65', 'Google Chrome', 'department_list.php'),
(13, '::1', '2015-05-20 22:11:53', '65', 'Google Chrome', 'department_list.php'),
(14, '::1', '2015-05-20 22:12:05', '65', 'Google Chrome', 'staff_profile.php'),
(15, '::1', '2015-05-20 22:12:12', '65', 'Google Chrome', 'department_list.php'),
(16, '::1', '2015-05-20 22:12:14', '65', 'Google Chrome', 'staff_profile.php'),
(17, '::1', '2015-05-20 22:12:17', '65', 'Google Chrome', 'staff_profile.php'),
(18, '::1', '2015-05-20 22:12:31', '65', 'Google Chrome', 'staff_profile.php'),
(19, '::1', '2015-05-20 22:12:42', '65', 'Google Chrome', 'department_list.php'),
(20, '::1', '2015-05-20 22:12:46', '65', 'Google Chrome', 'staff_profile.php'),
(21, '::1', '2015-05-20 22:12:48', '65', 'Google Chrome', 'staff_profile.php'),
(22, '::1', '2015-05-20 22:12:57', '65', 'Google Chrome', 'department_list.php'),
(23, '::1', '2015-05-20 22:12:59', '65', 'Google Chrome', 'department_list.php'),
(24, '::1', '2015-05-20 22:13:10', '65', 'Google Chrome', 'department_list.php'),
(25, '::1', '2015-05-20 22:13:55', '65', 'Google Chrome', 'publication_list.php'),
(26, '::1', '2015-05-20 22:13:59', '65', 'Google Chrome', 'publication_list.php'),
(27, '::1', '2015-05-20 22:14:49', '65', 'Google Chrome', 'publication_list.php'),
(28, '::1', '2015-05-20 22:15:32', '65', 'Google Chrome', 'article_category.php'),
(29, '::1', '2015-05-20 22:15:33', '65', 'Google Chrome', 'article_list.php'),
(30, '::1', '2015-05-20 22:15:50', '65', 'Google Chrome', 'article_list.php'),
(31, '::1', '2015-05-20 22:15:56', '65', 'Google Chrome', 'article_list.php'),
(32, '::1', '2015-05-20 22:58:55', '65', 'Google Chrome', 'user_privilege.php'),
(33, '::1', '2015-05-20 22:58:57', '65', 'Google Chrome', 'user_privilege.php'),
(34, '::1', '2015-05-20 22:59:17', '65', 'Google Chrome', 'framework_module.php'),
(35, '::1', '2015-05-20 22:59:37', '65', 'Google Chrome', 'framework_module.php'),
(36, '::1', '2015-05-20 22:59:39', '65', 'Google Chrome', 'middle.php'),
(37, '::1', '2015-05-20 22:59:45', '65', 'Google Chrome', 'framework_page.php'),
(38, '::1', '2015-05-20 23:00:34', '65', 'Google Chrome', 'framework_page.php'),
(39, '::1', '2015-05-20 23:00:39', '65', 'Google Chrome', 'user_privilege.php'),
(40, '::1', '2015-05-20 23:00:41', '65', 'Google Chrome', 'user_privilege.php'),
(41, '::1', '2015-05-20 23:00:44', '65', 'Google Chrome', 'user_privilege.php'),
(42, '::1', '2015-05-20 23:00:45', '65', 'Google Chrome', 'middle.php'),
(43, '::1', '2015-05-20 23:00:48', '65', 'Google Chrome', 'coordinator_list.php'),
(44, '::1', '2015-05-20 23:00:55', '65', 'Google Chrome', 'staff_profile.php'),
(45, '::1', '2015-05-20 23:00:59', '65', 'Google Chrome', 'staff_profile.php'),
(46, '::1', '2015-05-20 23:01:05', '65', 'Google Chrome', 'coordinator_list.php'),
(47, '::1', '2015-05-20 23:01:07', '65', 'Google Chrome', 'coordinator_list.php'),
(48, '::1', '2015-05-20 23:01:16', '65', 'Google Chrome', 'coordinator_list.php'),
(49, '::1', '2015-05-20 23:22:59', '65', 'Google Chrome', 'article_list.php'),
(50, '::1', '2015-05-20 23:23:01', '65', 'Google Chrome', 'article_list.php'),
(51, '127.0.0.1', '2015-05-20 23:23:19', '65', 'Google Chrome', 'article_list.php'),
(52, '127.0.0.1', '2015-05-20 23:23:20', '65', 'Google Chrome', 'article_list.php'),
(53, '::1', '2015-05-20 23:23:58', '65', 'Google Chrome', 'article_list.php'),
(54, '::1', '2015-05-20 23:24:06', '65', 'Google Chrome', 'article_list.php'),
(55, '::1', '2015-05-20 23:26:35', '65', 'Google Chrome', 'department_list.php'),
(56, '::1', '2015-05-20 23:26:43', '65', 'Google Chrome', 'user_privilege.php'),
(57, '::1', '2015-05-20 23:26:45', '65', 'Google Chrome', 'user_privilege.php'),
(58, '::1', '2015-05-20 23:26:50', '65', 'Google Chrome', 'user_privilege.php'),
(59, '::1', '2015-05-20 23:26:51', '65', 'Google Chrome', 'middle.php'),
(60, '::1', '2015-05-20 23:26:54', '65', 'Google Chrome', 'school_list.php'),
(61, '::1', '2015-05-20 23:26:58', '65', 'Google Chrome', 'school_list.php'),
(62, '::1', '2015-05-20 23:27:03', '65', 'Google Chrome', 'school_list.php'),
(63, '::1', '2015-05-20 23:27:16', '65', 'Google Chrome', 'school_list.php'),
(64, '::1', '2015-05-20 23:27:20', '65', 'Google Chrome', 'school_list.php'),
(65, '::1', '2015-05-20 23:33:43', '65', 'Google Chrome', 'article_list.php'),
(66, '::1', '2015-05-20 23:33:51', '65', 'Google Chrome', 'article_category.php'),
(67, '::1', '2015-05-20 23:33:57', '65', 'Google Chrome', 'log_history.php'),
(68, '::1', '2015-05-20 23:34:14', '65', 'Google Chrome', 'middle.php'),
(69, '::1', '2015-05-20 23:34:25', '65', 'Google Chrome', 'block_ip.php'),
(70, '::1', '2015-05-20 23:34:28', '65', 'Google Chrome', 'backup.php'),
(71, '::1', '2015-05-20 23:34:30', '65', 'Google Chrome', 'user_account.php'),
(72, '::1', '2015-05-20 23:34:46', '65', 'Google Chrome', 'framework_show.php'),
(73, '::1', '2015-05-20 23:37:19', '65', 'Google Chrome', 'framework_show.php'),
(74, '::1', '2015-05-20 23:37:20', '65', 'Google Chrome', 'framework_show.php'),
(75, '::1', '2015-05-20 23:37:32', '65', 'Google Chrome', 'framework_page.php'),
(76, '::1', '2015-05-20 23:37:33', '65', 'Google Chrome', 'framework_show.php'),
(77, '::1', '2015-05-20 23:37:35', '65', 'Google Chrome', 'framework_module.php'),
(78, '::1', '2015-05-20 23:37:38', '65', 'Google Chrome', 'framework_show.php'),
(79, '::1', '2015-05-20 23:37:47', '65', 'Google Chrome', 'research_theme.php'),
(80, '::1', '2015-05-20 23:37:57', '65', 'Google Chrome', 'research_theme.php'),
(81, '::1', '2015-05-20 23:38:15', '65', 'Google Chrome', 'department_list.php'),
(82, '::1', '2015-05-20 23:38:28', '65', 'Google Chrome', 'school_list.php'),
(83, '::1', '2015-05-20 23:38:36', '65', 'Google Chrome', 'Institution.php'),
(84, '::1', '2015-05-21 00:08:19', '65', 'Google Chrome', 'middle.php'),
(85, '::1', '2015-05-21 00:08:23', '65', 'Google Chrome', 'article_category.php'),
(86, '::1', '2015-05-21 00:08:41', '65', 'Google Chrome', 'article_category.php'),
(87, '::1', '2015-05-21 00:08:43', '65', 'Google Chrome', 'article.php'),
(88, '::1', '2015-05-21 00:09:25', '65', 'Google Chrome', 'article.php'),
(89, '::1', '2015-05-21 00:09:32', '65', 'Google Chrome', 'article_list.php'),
(90, '::1', '2015-05-21 00:09:36', '65', 'Google Chrome', 'article_list.php'),
(91, '::1', '2015-05-21 00:10:17', '65', 'Google Chrome', 'article_list.php'),
(92, '::1', '2015-05-21 00:10:17', '65', 'Google Chrome', 'article_list.php'),
(93, '::1', '2015-05-21 11:19:49', '65', 'Google Chrome', 'article_list.php'),
(94, '::1', '2015-05-21 11:25:10', '65', 'Google Chrome', 'article_list.php'),
(95, '::1', '2015-05-21 11:30:42', '65', 'Google Chrome', 'article_list.php'),
(96, '::1', '2015-05-21 11:31:05', '65', 'Google Chrome', 'article_list.php'),
(97, '::1', '2015-05-21 11:40:48', '65', 'Google Chrome', 'article_list.php'),
(98, '::1', '2015-05-21 11:40:53', '65', 'Google Chrome', 'article_list.php'),
(99, '::1', '2015-05-21 11:53:13', '65', 'Google Chrome', 'article_list.php'),
(100, '::1', '2015-05-21 11:53:24', '65', 'Google Chrome', 'article_list.php'),
(101, '::1', '2015-05-21 12:01:11', '65', 'Google Chrome', 'article_list.php'),
(102, '::1', '2015-05-21 12:01:31', '65', 'Google Chrome', 'article_list.php'),
(103, '::1', '2015-05-21 12:01:55', '65', 'Google Chrome', 'article_list.php'),
(104, '::1', '2015-05-21 12:02:00', '65', 'Google Chrome', 'article_list.php'),
(105, '::1', '2015-05-21 12:02:48', '65', 'Google Chrome', 'article_list.php'),
(106, '::1', '2015-05-21 12:02:53', '65', 'Google Chrome', 'article_list.php'),
(107, '::1', '2015-05-21 12:02:59', '65', 'Google Chrome', 'article_list.php'),
(108, '::1', '2015-05-21 12:03:21', '65', 'Google Chrome', 'article_list.php'),
(109, '::1', '2015-05-21 12:09:56', '65', 'Google Chrome', 'coordinator_list.php'),
(110, '::1', '2015-05-21 12:10:05', '65', 'Google Chrome', 'coordinator_list.php'),
(111, '::1', '2015-05-21 12:10:43', '65', 'Google Chrome', 'coordinator_list.php'),
(112, '::1', '2015-05-21 12:10:46', '65', 'Google Chrome', 'coordinator_list.php'),
(113, '::1', '2015-05-21 12:12:42', '65', 'Google Chrome', 'coordinator_list.php'),
(114, '::1', '2015-05-21 12:13:26', '65', 'Google Chrome', 'coordinator_list.php'),
(115, '::1', '2015-05-21 12:13:37', '65', 'Google Chrome', 'coordinator_list.php'),
(116, '::1', '2015-05-21 12:19:41', '65', 'Google Chrome', 'coordinator_list.php'),
(117, '::1', '2015-05-21 12:20:12', '65', 'Google Chrome', 'coordinator_list.php'),
(118, '::1', '2015-05-21 12:20:20', '65', 'Google Chrome', 'coordinator_list.php'),
(119, '::1', '2015-05-21 12:20:29', '65', 'Google Chrome', 'coordinator_list.php'),
(120, '::1', '2015-05-21 12:21:09', '65', 'Google Chrome', 'coordinator_list.php'),
(121, '::1', '2015-05-21 12:21:23', '65', 'Google Chrome', 'coordinator_list.php'),
(122, '::1', '2015-05-21 12:21:27', '65', 'Google Chrome', 'coordinator_list.php'),
(123, '::1', '2015-05-21 12:21:34', '65', 'Google Chrome', 'coordinator_list.php'),
(124, '::1', '2015-05-21 12:21:56', '65', 'Google Chrome', 'coordinator_list.php'),
(125, '::1', '2015-05-21 12:22:10', '65', 'Google Chrome', 'coordinator_list.php'),
(126, '::1', '2015-05-21 12:22:23', '65', 'Google Chrome', 'coordinator_list.php'),
(127, '::1', '2015-05-21 12:22:29', '65', 'Google Chrome', 'coordinator_list.php'),
(128, '::1', '2015-05-21 12:26:52', '65', 'Google Chrome', 'article_list.php'),
(129, '::1', '2015-05-21 12:27:33', '65', 'Google Chrome', 'article_list.php'),
(130, '::1', '2015-05-21 12:27:39', '65', 'Google Chrome', 'article_list.php'),
(131, '::1', '2015-05-21 12:27:41', '65', 'Google Chrome', 'article_list.php'),
(132, '::1', '2015-05-21 12:27:48', '65', 'Google Chrome', 'article_list.php'),
(133, '::1', '2015-05-21 12:37:26', '65', 'Google Chrome', 'article_category.php'),
(134, '::1', '2015-05-21 12:37:29', '65', 'Google Chrome', 'article_category.php'),
(135, '::1', '2015-05-21 12:37:30', '65', 'Google Chrome', 'article_list.php'),
(136, '::1', '2015-05-21 12:37:32', '65', 'Google Chrome', 'article_list.php'),
(137, '::1', '2015-05-21 12:39:18', '65', 'Google Chrome', 'article_list.php'),
(138, '::1', '2015-05-21 12:48:09', '65', 'Google Chrome', 'article_category.php'),
(139, '::1', '2015-05-21 12:48:17', '65', 'Google Chrome', 'article_category.php'),
(140, '::1', '2015-05-21 12:50:04', '65', 'Google Chrome', 'article_list.php'),
(141, '::1', '2015-05-21 12:50:05', '65', 'Google Chrome', 'article_list.php'),
(142, '::1', '2015-05-21 12:50:51', '65', 'Google Chrome', 'article_list.php');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_module`
--

CREATE TABLE IF NOT EXISTS `tbl_module` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ModuleName` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `ModuleDescription` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Url` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Icon` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Iconsmall` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `ModuleClass` varchar(255) NOT NULL,
  `leftdisplay` int(11) NOT NULL DEFAULT '1',
  `topdisplay` int(11) NOT NULL DEFAULT '1',
  `middledisplay` int(11) NOT NULL DEFAULT '1',
  `torder` int(11) NOT NULL,
  `Category` varchar(255) NOT NULL DEFAULT 'Backend',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `tbl_module`
--

INSERT INTO `tbl_module` (`ID`, `ModuleName`, `ModuleDescription`, `Url`, `Icon`, `Iconsmall`, `ModuleClass`, `leftdisplay`, `topdisplay`, `middledisplay`, `torder`, `Category`) VALUES
(1, 'Blog', 'Blog', 'blog', 'blog', 'blog', 'blog', 1, 1, 1, 0, 'Frontend'),
(2, 'Services', 'service', 'service', 'service', 'service', 'service', 0, 0, 0, 0, 'Frontend'),
(4, 'Settings', 'Settings', 'Settings', 'configuration.png', 'key.png', 'fa fa-gear fa-fw', 1, 0, 1, 8, 'Backend'),
(5, 'Framework', 'App Manager', '', '', '', 'fa fa-wrench fa-fw', 1, 1, 1, 9, 'Backend'),
(9, 'Translation', 'Translation', '', '', '', '', 1, 1, 1, 6, 'Backend'),
(10, 'Articles', 'Articles', '', '', '', '', 1, 1, 1, 7, 'Backend'),
(14, 'MyPublications', 'MyPublications', 'MyPublications', 'MyPublications', 'MyPublications', 'MyPublications', 1, 1, 1, 15, 'Backend'),
(15, 'College Profile', 'College Profile', 'CollegeProfile', 'CollegeProfile', 'CollegeProfile', 'CollegeProfile', 1, 1, 1, 15, 'Backend'),
(16, 'School Profile', 'School Profile', '', '', '', '', 1, 1, 1, 16, 'Backend'),
(17, 'Department Profile', 'Department Profile', '', '', '', '', 1, 1, 1, 17, 'Backend'),
(18, 'My Profile', 'My Profile', '', '', '', '', 1, 1, 1, 18, 'Backend'),
(19, 'Alumn', 'Alumn', '', '', '', '', 1, 1, 1, 0, 'Backend'),
(20, 'Research', 'fyp', 'fyp', 'fyp', 'fyp', 'fyp', 1, 1, 1, 20, 'Backend'),
(21, 'Work load Policy', 'Work load Policy', 'wlp', 'wlp', 'wlp', 'wlp', 1, 1, 1, 30, 'Backend'),
(22, 'FYP', 'FYP', 'FYP', 'FYP', 'FYP', 'fyp', 1, 1, 1, 30, 'Backend'),
(23, 'Coordinator', 'Coordinator', 'Coordinator', 'Coordinator', 'Coordinator', 'Coordinator', 1, 1, 1, 5, 'Backend');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_newsletter_address`
--

CREATE TABLE IF NOT EXISTS `tbl_newsletter_address` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(255) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_newsletter_address`
--

INSERT INTO `tbl_newsletter_address` (`Id`, `Email`, `Status`) VALUES
(2, 'deoshayo@hotmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_organization`
--

CREATE TABLE IF NOT EXISTS `tbl_organization` (
  `Name` varchar(255) NOT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Tel` varchar(255) NOT NULL,
  `Fax` varchar(255) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Website` varchar(255) NOT NULL,
  `City` varchar(255) NOT NULL,
  `Active` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_organization`
--

INSERT INTO `tbl_organization` (`Name`, `Id`, `Tel`, `Fax`, `Address`, `Email`, `Website`, `City`, `Active`) VALUES
('DVS - HQ', 1, '+2557160865454', '+2557160865454', 'Dodoma', 'hq@hakikidawa.org', 'www.hakikidawa.org', 'Dodoma', 1),
('DVS - Eastern Zone', 2, '+2557160865454', '+2557160865454', 'Da es Salaam', 'east@hakikidawa.org', 'www.hakikidawa.org', 'Dar es Salaam', 0),
('DVS - Mwanza', 5, '08474646454', '833863636', 'Mwanza', 'msd@gmail.com', 'mwanza.hakikidawa.org', 'MWANZA', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_portal_content`
--

CREATE TABLE IF NOT EXISTS `tbl_portal_content` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title_en` varchar(255) NOT NULL,
  `Title_sw` varchar(255) NOT NULL,
  `SectionName` varchar(255) NOT NULL,
  `Content_sw` text NOT NULL,
  `Content_en` text NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  `Language` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `SectionName` (`SectionName`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Content Manager' AUTO_INCREMENT=35 ;

--
-- Dumping data for table `tbl_portal_content`
--

INSERT INTO `tbl_portal_content` (`Id`, `Title_en`, `Title_sw`, `SectionName`, `Content_sw`, `Content_en`, `Status`, `Language`) VALUES
(1, 'Our Goals', 'Malengo yetu', 'Goal', '<p>Tumedhamiria kukomesha hili tatizo</p>', '<p>Tanzania become united in combating drug counterfeit.We then add customized hygiene education services and broker key relationships to solve sanitation challenges. By piggybacking on technology and supply chains already in use by major food and hotel chains, weï¿½re able to set a bold goal of 100% coverage of a platform (instead of simply a pilot project): every orphanage in China; every public school in Kathmandu, Nepal; every child-serving institution in Kolkata, India; and more.</p>', 1, 1),
(2, 'Verification', 'Uhakiki wa Madawa', 'Verification', '<p>Uhakiki wa ubora dawa</p>', '<p>Verifying the genuiness of medical drug</p>', 1, 1),
(3, 'Supply Chain Management', 'Mnyororo wa Usambazaji', 'SupplyChain', 'Mnyororo wa Usambazaji', 'Supply chain management', 1, 1),
(4, 'Data Analytics', 'Uchambuzi wa Dataz', 'DataAnalytics', '<p>Uchambuzi wa Data za madawa</p>', '<p>Data Analytics services</p>', 1, 1),
(5, 'Featured News', 'Habari', 'FeaturedNews', 'Habari Mbalimbali', 'Featured News', 1, 1),
(6, 'Verification Counts', 'Takwimu za Uhakiki', 'VerificationCounts', 'Kila tarakimu ina wakilisha idadi ya mteja aliyehakiki dawa yake', 'Each verification represents a consumer becoming empowered to protect herself from potentially harmful counterfeits.', 1, 1),
(7, 'Follow us on social media', 'Tufuate katika mitandao ya kijamii', 'SocialMedia', 'Tufuate katika mitandao ya kijamii', 'Follow us on social media', 1, 1),
(8, 'Newsletter', 'Makala', 'NewsLetter', 'Sajili anuani yako kupokea makala zetu', 'Register your email address to recieve our newsletter', 1, 1),
(9, 'Overview of our Services', 'Maelezo ya Jumla ya Huduma zetu', 'services_overview', 'HAKIKI  DAWA ni mradi ulioshinda tuzo ya kufanya utafiti wa matumizi ya teknolojia Tanzania', 'HAKIKI DAWA uses mobile technology to combat counterfeiting. Its award-winning R&D project by TCRA', 1, 1),
(10, 'Our service focus on the health of the medical drugs conusmers', 'Huduma  zetu zinalenga katika kulika afya ya watumiaji wa <b>madawa</b>', 'services_overview_list', '<ul class="col-xs-6 list-unstyled lists-v1">\r\n <li><i class="fa fa-angle-right"></i>Uhakiki kwa SMS</li>\r\n <li><i class="fa fa-angle-right"></i>Uhakiki kwa smartphone</li>\r\n <li><i class="fa fa-angle-right"></i>Uhakiki kwa Tovuti</li>\r\n<li><i class="fa fa-angle-right"></i>Ufutiliaji wa madawa</li>\r\n</ul>\r\n                   ', '\r\n\r\n<ul class="col-xs-6 list-unstyled lists-v1">\r\n <li><i class="fa fa-angle-right"></i>SMS based verification</li>\r\n <li><i class="fa fa-angle-right"></i>Smartphone based verification</li>\r\n <li><i class="fa fa-angle-right"></i>Web based verifications</li>\r\n<li><i class="fa fa-angle-right"></i>Track and Trace verification</li>\r\n</ul>', 1, 1),
(11, 'Our Documentary', 'Panorama Yetu', 'Documentary', 'Tazama panorama ya mradi wetu', 'Whatch our documentary', 1, 1),
(15, 'Step One', 'Hatua ya Kwaza', 'SmartVerify1', '<p>Pakua&nbsp;app yako ya android kutoka google stoo, kisha kutafuta DVS baada ya kufunga&nbsp;app au kama una ni wazi kwa kubonyeza maombi icon katika smart yako admin simu na kuufungua.</p>', '<p><span style="color: #555555; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 20.7999992370605px;">Download your android application from google play store,then search DVS after installing the application or if you have it open by clicking the application icon in your android smart phone to open it.</span></p>', 1, 1),
(12, 'About Us', 'Kuhusu Sisi', 'About', 'Kikundi cha watafiti kwenye madawa', 'We are research group dealing with drugs informatics', 1, 1),
(13, 'clean', 'safi', 'Futa', 'kweli nzuri', 'sure good', 1, 1),
(14, 'Report a case', 'Ripoti Suala', 'contact', '<p>Ripoti kwetu suala lolote linahusina na ubora wa madawa katika eneo lako</p>', '<p>Report any case regarding medical drugs quality in your area</p>', 1, 1),
(16, 'Step Two', 'Hatua ya Pili', 'SmartVerify2', '<p>Baada ya kufungua programu utakuwa na interface kama katika picha hapo juu click kwenye uwanja asilia na kuingia code kutoka Drag yako ya matibabu.</p>', '<p><span style="color: #555555; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 20.7999992370605px;">After opening th application you will have the interface like in the image above click on the text field and enter the code from your medical drag.</span></p>', 1, 1),
(17, 'Step Three', 'Hatua ya Tatu', 'SmartVerify3', '<p>Kisha kusubiri kwa matokeo kutoka server, baada ya baadhi ya dakika utapata maoni kutoka server kuhusu madawa ya kulevya, alijaribu kuthibitisha kama madawa ya kulevya ni ya kweli unaweza kuendelea na matumizi ya kawaida vinginevyo ni bandia utakuwa kupokea taarifa juu ya nini kufanya ijayo.</p>', '<p><span style="color: #555555; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 20.7999992370605px;">Then wait for the results from the server,after some minutes you will receive feedback from the server about the drug you tried to verify if the drug is genuine you can continue with the normal usage otherwise it is a counterfeit you will receive a notification about what to do next.</span></p>', 1, 1),
(18, 'Verification via Android Smartphone', 'Uhakiki kupitia Smartphone Android', 'SmartVerify0', '<p>Jinsi ya kutumia</p>', '<p>How to Use</p>', 1, 1),
(19, 'Verification via Featured Phone', 'Uhakiki kupitia Simu za Kawaida', 'NativeVerify0', '<p>Jinsi ya kutumia</p>', '<p>How to Use</p>', 1, 1),
(20, 'Step One', 'Hatua ya Kwaza', 'NativeVerify1', '<p>Hii ni rahisi ya yote, wewe tu haja ya kuwa na simu yako lakini kabla ya kutumia hakikisha kuwa scratched ukaguzi code yako tayari kuthibitisha.</p>', '<p><span style="color: #333333; font-family: Times; font-size: 19.2000007629395px; line-height: 19.2000007629395px; text-align: justify;">This is the simplest of all,you just need to have your phone but before using it make sure you have scratched you verification code ready to verify.</span></p>', 1, 1),
(21, 'Step Two', 'Hatua ya Pili', 'NativeVerify2', '<p>Kutokana wewe tayari scratched nambari yako ya kutoka dawa yako ya matibabu utakuwa na piga namba zifuatazo * 150 * 999 # na kufuata mafundisho zinazotolewa kutoka operator</p>', '<p><span style="color: #333333; font-family: Times; font-size: 19.2000007629395px; line-height: 19.2000007629395px; text-align: justify;">Assuming you have already scratched your verification code from your medical drug the you will have to dial the following numbers *150*999# and follow the instruction provided from the operator</span></p>', 1, 1),
(22, 'Step Three', 'Hatua ya Tatu', 'NativeVerify3', '<p>Kisha kusubiri kwa matokeo kutoka server, baada ya baadhi ya dakika utapata maoni kutoka server kuhusu madawa ya kulevya, alijaribu kuthibitisha kama madawa ya kulevya ni ya kweli unaweza kuendelea na matumizi ya kawaida vinginevyo ni bandia utakuwa kupokea taarifa juu ya nini kufanya ijayo.</p>', '<p><span style="color: #333333; font-family: Times; font-size: 19.2000007629395px; line-height: 19.2000007629395px; text-align: justify;">Then wait for the results from the server,after some minutes you will recieve feedback from the server about the drug you tried to verify if the drug is genuine you can continue with the normal usage otherwise it is a counterfeit you will receive a notification about what to do next.</span></p>', 1, 1),
(23, 'Verification via Web portal', 'Uhakiki kupitia Tovuti', 'WebVerify0', '<p>Jinsi ya kutumia</p>', '<p style="text-align: justify;"><span style="color: #333333; font-family: Times;"><span style="font-size: 19.2000007629395px; line-height: 19.2000007629395px;">How to Use</span></span></p>', 1, 1),
(27, 'OUR PROCESS', 'MCHAKATO WETU', 'OurProcess', '<p>Sisi kutoa jukwaa kwa ajili ya kufuatilia madawa ya matibabu batches kutoka hatua ya viwanda kwa uhakika kufikia matumizi</p>', '<p>We provide platiform for tracking medical drugs batches from the point of manufacturing to the point it reach the consumer</p>', 1, 1),
(24, 'Step One', 'Hatua ya Kwaza', 'WebVerify1', '<p>Fungua favorite mtandao browser yako yaani Google Chrome, Mozila Firefox au Safari kuingia kiungo zifuatazo www.dvs.com kuhakikisha kifaa yako ni kushikamana na mtandao.</p>', '<p>Open your favorite web browser i.e Google Chrome, Mozila firefox or Safari the enter the following link www.dvs.com make sure your device is connected to the internet.</p>', 1, 1),
(25, 'Step Two', 'Hatua ya Pili', 'WebVerify2', '<p>Baada ya kufungua webpage utakuwa interface kama katika picha hapo juu click kwenye uwanja asilia na kuingia code kutoka Drag yako ya matibabu tayari kuthibitisha.</p>', '<p><span style="color: #333333; font-family: Times; font-size: 19.2000007629395px; line-height: 19.2000007629395px; text-align: justify;">After opening the webpage you will have the interface like in the image above click on the text field and enter the code from your medical drag ready to verify.</span></p>', 1, 1),
(26, 'Step Three', 'Hatua ya Tatu', 'WebVerify3', '<p>Kisha kusubiri kwa matokeo kutoka server, baada ya baadhi ya dakika utapata maoni kutoka server kuhusu madawa ya kulevya, alijaribu kuthibitisha kama madawa ya kulevya ni ya kweli unaweza kuendelea na matumizi ya kawaida vinginevyo ni bandia utakuwa kupokea taarifa juu ya nini kufanya ijayo.</p>', '<p><span style="color: #333333; font-family: Times; font-size: 19.2000007629395px; line-height: 19.2000007629395px; text-align: justify;">Then wait for the results from the server,after some minutes you will receive feedback from the server about the drug you tried to verify if the drug is genuine you can continue with the normal usage otherwise it is a counterfeit you will receive a notification about what to do next.</span></p>', 1, 1),
(28, 'Manufacturer', 'Mtengenezaji', 'Manufacturer', '<p>Kupokea oda na kutoa wa madawa ya matibabu</p>', '<p>Receive order and supply the required medical drugs</p>', 1, 1),
(29, 'Distributor', 'Msambazaji', 'Distributor', '<p>Repackage vifaa na kusambaza kwa wauzaji wa jumla</p>', '<p>Repackage supplies and distribute to whole salers</p>', 1, 1),
(30, 'Supplier', 'Muuzaji', 'Supplier', '<p>Kupokea madawa ya matibabu kutoka wasambazaji, kuamsha masanduku na hawaoni madawa ya matibabu</p>', '<p>&nbsp;Receive medical drugs from distributors, activate the boxes and dispense medical drugs</p>', 1, 1),
(31, 'Consumer', 'Mtumiaji', 'Consumer', '<p>Kuthibitisha madawa ya matibabu kupitia SMS, program ya Smartphone au tovuti</p>', '<p>Verify medical drugs through SMS, Smartphone App or Web platform</p>', 1, 1),
(32, 'Welcome', 'Ukaribisho', 'PrincipalNote', '<p><span style="color: #777777; font-family: Muli, sans-serif; font-size: 16px; line-height: 26px;">Welcome to the College of Informatics and Virtual Education of the University of Dodoma. Strategically located in Dodoma city, the centre and the national capital of Tanzania, the college is destined to be a centre of excellence in ICT. Our mission will always be to provide comprehensive, gender sensitive and quality education to a broad segment of the population through teaching, research and public services in ICT. Blessed with competent, dedicated, self-motivated and highly skilled academic, technical and administrative staff, the college has been able to make remarkable achievements despite our young history. Since its establishment in 2008, the College has produced three batches of graduates with Certificate, Diploma and Degree qualifications in various specialties. Our graduates have demonstrated exceptional strengths and qualities that have amazed many. They have secured reputable jobs in the Central Government, Local Governments, Parastatal Organizations, Non-Governmental Organizations and Private Institutions within and outside the country. Some of our graduates have been bold enough to start their own companies. The College will continually strive to produce qualified and competitive graduates in specialized ICT areas. As we start new academic year, we expect each member of the college community to be innovative in all his/her endeavor and to originate new ideas which are vital ingredients in any live and vibrant academic institution. I wish you all a prosperous new academic year 2013-2014.</span></p>', '<p><span style="color: #777777; font-family: Muli, sans-serif; font-size: 16px; line-height: 26px;">Welcome to the College of Informatics and Virtual Education of the University of Dodoma. Strategically located in Dodoma city, the centre and the national capital of Tanzania, the college is destined to be a centre of excellence in ICT. Our mission will always be to provide comprehensive, gender sensitive and quality education to a broad segment of the population through teaching, research and public services in ICT. Blessed with competent, dedicated, self-motivated and highly skilled academic, technical and administrative staff, the college has been able to make remarkable achievements despite our young history. Since its establishment in 2008, the College has produced three batches of graduates with Certificate, Diploma and Degree qualifications in various specialties. Our graduates have demonstrated exceptional strengths and qualities that have amazed many. They have secured reputable jobs in the Central Government, Local Governments, Parastatal Organizations, Non-Governmental Organizations and Private Institutions within and outside the country. Some of our graduates have been bold enough to start their own companies. The College will continually strive to produce qualified and competitive graduates in specialized ICT areas. As we start new academic year, we expect each member of the college community to be innovative in all his/her endeavor and to originate new ideas which are vital ingredients in any live and vibrant academic institution. I wish you all a prosperous new academic year 2013-2014.</span></p>', 1, 1),
(33, 'School of Informatics', 'Shule ya Informatikia', 'DeanSoiNote', '<p><span style="color: #777777; font-family: Muli, sans-serif; font-size: 16px; line-height: 26px;">Welcome to the School of Informatics of the University of Dodoma. The School of Informatics is expecting to support the College of Informatics and Virtual Education and University of Dodoma to realise the vision of being Centre of Excellence in Information and Communication Technology. The School is striving to build competences in various areas of ICT. The School of Informatics is among of the first Schools at the University of Dodoma, it was established in 2007. Since its establishment the School has distinguished itself to come up with various innovative Degree, Diploma and Certificate programmes.</span></p>', '<p><span style="color: #777777; font-family: Muli, sans-serif; font-size: 16px; line-height: 26px;">Welcome to the School of Informatics of the University of Dodoma. The School of Informatics is expecting to support the College of Informatics and Virtual Education and University of Dodoma to realise the vision of being Centre of Excellence in Information and Communication Technology. The School is striving to build competences in various areas of ICT. The School of Informatics is among of the first Schools at the University of Dodoma, it was established in 2007. Since its establishment the School has distinguished itself to come up with various innovative Degree, Diploma and Certificate programmes.</span></p>', 1, 1),
(34, 'School of Virtual Education', 'Shule ya Masafa', 'DeanSoveNote', '<p><span style="color: #777777; font-family: Muli, sans-serif; font-size: 16px; line-height: 26px;">Welcome to the School of Informatics of the University of Dodoma. The School of Informatics is expecting to support the College of Informatics and Virtual Education and University of Dodoma to realise the vision of being Centre of Excellence in Information and Communication Technology. The School is striving to build competences in various areas of ICT. The School of Informatics is among of the first Schools at the University of Dodoma, it was established in 2007. Since its establishment the School has distinguished itself to come up with various innovative Degree, Diploma and Certificate programmes.</span></p>', '<p><span style="color: #777777; font-family: Muli, sans-serif; font-size: 16px; line-height: 26px;">Welcome to the School of Informatics of the University of Dodoma. The School of Informatics is expecting to support the College of Informatics and Virtual Education and University of Dodoma to realise the vision of being Centre of Excellence in Information and Communication Technology. The School is striving to build competences in various areas of ICT. The School of Informatics is among of the first Schools at the University of Dodoma, it was established in 2007. Since its establishment the School has distinguished itself to come up with various innovative Degree, Diploma and Certificate programmes.</span></p>', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_portal_label`
--

CREATE TABLE IF NOT EXISTS `tbl_portal_label` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `LabelName` varchar(255) NOT NULL,
  `Title_en` varchar(255) NOT NULL,
  `Title_sw` varchar(255) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `LabelName` (`LabelName`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `tbl_portal_label`
--

INSERT INTO `tbl_portal_label` (`Id`, `LabelName`, `Title_en`, `Title_sw`, `Status`) VALUES
(8, 'Copyrights', 'All Rights Reserved', 'Haki zote zimehifadhiwa', 1),
(6, 'Login', 'Login', 'Ingia', 1),
(7, 'Help', 'Help', 'Usaidizi', 1),
(9, 'PrivacyPolicy', 'Privacy Policy', 'Sera ya Faraga', 1),
(10, 'TermsService', 'Terms of Service', 'Masharti na Vigezo', 1),
(12, 'Search', 'Search', 'Pekua', 1),
(13, 'Verify', 'Verify', 'Hakiki', 1),
(14, 'Contact', 'Contacts', 'Mawasiliano', 1),
(15, 'FeaturedNews', 'Featured News', 'Habari', 1),
(16, 'Blog', 'Blog', 'Blogu', 1),
(17, 'Service', 'Services', 'Huduma', 1),
(18, 'Overview', 'Overview', 'Muhtasari', 1),
(19, 'Verification', 'Verification', 'Ukaguzi', 1),
(20, 'FacilitySearch', 'Facility Search', 'Tafuta Kituo cha Huduma', 1),
(21, 'DataAnalytics', 'DataAnalytics', 'Uchambuzi wa Data', 1),
(22, 'OrderOnline', 'Order Online', 'Agiza Mtandaoni', 1),
(23, 'Languages', 'Languages', 'Lugha', 1),
(24, 'readmore', 'read more', 'Soma zaidi', 1),
(25, 'RecentPost', 'Recent Posts', 'Machapisho Mapya', 1),
(26, 'SeeAllPosts', 'See all posts', 'Angalia machapisho yote', 1),
(27, 'Posted', 'Posted', 'Imewekwa', 1),
(28, 'Comment', 'Comments', 'Maoni', 1),
(29, 'LeaveComment', 'Leave a Comment', 'Toa Maoni', 1),
(30, 'Name', 'Name', 'Jina', 1),
(31, 'Email', 'Email', 'Barua Pepe', 1),
(32, 'Submit', 'Submit', 'Wasilisha', 1),
(33, 'BlogTag', 'Blog Tags', 'Aina za Machapisho', 1),
(34, 'LearnMore', 'LearnMore', 'Jifunze zaidi', 1),
(35, 'HealthFacilitySearch', 'Healthcare Facility Search', 'Tafuta Kituo cha Afya', 1),
(36, 'SearchFor', 'Search for', 'Utafutaji wa', 1),
(37, 'ResultFound', 'Result Found', 'Matokeo yaliyopatikana', 1),
(38, 'Phone', 'Phone', 'Simu', 1),
(39, 'Link', 'LInk', 'Kiunganishi', 1),
(40, 'CallEmail', 'Call or Email', 'Piga Simu au Tuma Barua pepe', 1),
(41, 'Website', 'Website', 'Tovuti', 1),
(42, 'UsefulLinks', 'Useful Links', 'Viunganishi Muhimu', 1),
(43, 'SearchText', 'Search words with regular expressions ...', 'Andika neno la kupekulia', 1),
(44, 'Close', 'Close', 'Funga', 1),
(45, 'Dosage', 'Dosage', 'Dozi', 1),
(46, 'SideEffect', 'Side Effects', 'Madhara', 1),
(47, 'Interaction', 'Interaction', 'Mchanganyo', 1),
(48, 'PriceInformation', 'Price Information', 'Maelezo ya bei', 1),
(49, 'Drug', 'Drug', 'Dawa', 1),
(50, 'Profile', 'Profile', 'Kabrasha', 1),
(51, 'SMSVerify', 'SMS-based verifications', 'Uhakiki kwa SMS', 1),
(52, 'SmartVerify', 'Smartphone-based verifications', 'Uhakiki kwa Smartphone', 1),
(53, 'WebVerify', 'Web-based verifications', 'Uhakiki kwa tovuti', 1),
(54, 'Statistics', 'Statistics', 'Takwimu', 1),
(55, 'TotalStatistics', 'Total Statistics', 'Jumla ya Takwimu', 1),
(56, 'PlatformVerification', 'Verifications Counts by Platiform', 'Takwimu za Uhakiki kwa jukwaa', 1),
(57, 'RegionVerification', 'Verifications Counts by Region', 'Takwimu za Uhakiki kwa Mikoa', 1),
(58, 'AboutUs', 'About Us', 'Kuhusu Sisi', 1),
(59, 'Home', 'Home', 'Maskani', 1),
(60, 'CaptchaText', 'Enter the numbers displayed in the image', 'Ingiza namba zinazonekana katika picha', 1),
(61, 'ScratchText', 'Enter Scratch Code', 'Ingiza Namba za Siri za dawa', 1),
(62, 'Message', 'Message', 'Ujumbe', 1),
(63, 'Subject', 'Subject', 'Kichwa cha Ujumbe', 1),
(64, 'ReportIssue', 'Report', 'Tutaarifu', 1),
(65, 'MedicalDrugProfile', 'Medical Drug Profiles', 'Taarifa za Madawa', 1),
(66, 'TopSmartVerify', 'Your Smartphone <br/> can save your life', 'Smartphone yako <br/> inaweza kuokoa maisha yako', 1),
(67, 'TopSMSVerify', 'Text Message in your  cellphone  <br/> can give you assurance', 'Ujumbe wa simu yako ya mkononi  <br/> unaweza kukupa uhakika', 1),
(68, 'TopWebVerify', 'Through this web platform <br/> you can get assured', 'Kupitia tovuti hii  <br/> unaweza kupata uhakika', 1),
(69, 'Announcement', 'Announcement', 'Matangazo', 1),
(70, 'News', 'News', 'Habari', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_privilege`
--

CREATE TABLE IF NOT EXISTS `tbl_privilege` (
  `priorityId` int(11) NOT NULL,
  `ModuleID` int(11) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`priorityId`,`ModuleID`),
  KEY `ModuleID` (`ModuleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_privilege`
--

INSERT INTO `tbl_privilege` (`priorityId`, `ModuleID`, `Status`) VALUES
(1, 4, 1),
(1, 5, 1),
(1, 10, 1),
(1, 14, 1),
(1, 15, 1),
(1, 16, 1),
(1, 17, 1),
(1, 19, 1),
(1, 20, 1),
(1, 22, 1),
(1, 23, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_program`
--

CREATE TABLE IF NOT EXISTS `tbl_program` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `acronym` varchar(100) NOT NULL,
  `Description` text NOT NULL,
  `StudyLevel` int(11) NOT NULL DEFAULT '1',
  `DepartmentID` int(11) NOT NULL,
  `Duration` int(11) NOT NULL DEFAULT '3',
  `Status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `StudyLevel` (`StudyLevel`),
  KEY `StudyLevel_2` (`StudyLevel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `tbl_program`
--

INSERT INTO `tbl_program` (`Id`, `Name`, `acronym`, `Description`, `StudyLevel`, `DepartmentID`, `Duration`, `Status`) VALUES
(1, 'Bachelor of Science in Computer Science', 'CS', '<p>Accountable government recognizes the needs and interests of citizens and works to advance public welfare. Lack of accountability impedes both democracy and socio-economic development of any community. Citizen feedback is important for boosting government transparency and accountability. ICTs can help expanding the communication channel between government and citizen while at the same time foster transparency and accountability in the public services. This study proposes the use of hybrid ICTs as a tool to engage citizen in monitoring their government. Evaluation of the proposed prototype shows the use of ICTs in facilitating citizen engagement is still infancy and more research are needed to explore their feasibility in Tanzanian context.</p>', 3, 2, 3, 1),
(3, 'Bachelor of Science in Computer Information Security', 'CIS', 'Bachelor of Science in Computer Security', 3, 1, 3, 1),
(5, 'Bachelor of Science in Software Engineering', 'SE', '<p><span style="color: #333333; font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;"><span style="font-size: 14px; line-height: 20px; background-color: #f5f5f5;">asjh asjhjas &nbsp;ajshjhjash &nbsp;ajshjashh jhasjhas hhjahsjhjash &nbsp;asjhajs jas hajshhjash asj h</span></span></p>', 3, 2, 4, 1),
(6, 'Certificate In Computer Networks', 'CCN', '<p>dadsdsdsdsdsd</p>', 1, 2, 1, 1),
(7, 'Diploma In Systems Administration', 'DSA', '<p>sdsdadad</p>', 2, 2, 2, 1),
(8, 'Masters of Science in Computer Science', 'MscCS', '<p>dsdsdsd</p>', 4, 2, 2, 1),
(9, 'Bachelor of Science in Business Information System', 'BIS', '<p>dsdsd</p>', 3, 2, 3, 1),
(10, 'Bachelor of Science in Computer Engineering', 'CE', '<p>sdsdsd</p>', 3, 6, 3, 1),
(11, 'Bachelor of Science in Heath Information Systems', 'HIS', '', 3, 5, 3, 1),
(12, 'Bachelor of Science in Information Systems', 'IS', '', 3, 5, 3, 1),
(13, 'Bachelor of Science in Telecommunication', 'TE', '', 3, 7, 4, 1),
(14, 'Bachelor of Science in Multimedia Technology and Application', 'MTA', '', 3, 1, 3, 1),
(15, 'Bachelor of Science in ICT MCD', 'ICT MCD', '', 3, 1, 3, 1),
(16, 'DIPLOMA IN SYSTEMS ADMINISTRATION', 'DCSA', '', 2, 2, 2, 1),
(17, 'DIPLOMA IN ICT', 'DICT', '<p>SAAS</p>', 2, 2, 2, 1),
(18, 'DIPLOMA IN ICT WITH EDUCATION', 'DICT-ED', '<p>DSD</p>', 2, 1, 2, 1),
(19, 'DIPLOMA IN CS', 'DCS', '<p>SASAS</p>', 2, 2, 2, 1),
(20, 'CERTIFICATE IN ICT', 'CICT', '<p>SDDSD</p>', 1, 2, 1, 1),
(21, 'CERTIFICATE IN COMPUTER NETWORKS', 'CCN', '<p>CCCCXCC</p>', 1, 6, 1, 1),
(22, 'CERTIFICATE IN COMPUTER GRAPHICS DESIGN', 'CET', '<p>SDD</p>', 1, 6, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_publication`
--

CREATE TABLE IF NOT EXISTS `tbl_publication` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `StaffID` int(11) NOT NULL,
  `Category` varchar(255) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Publisher` varchar(255) NOT NULL,
  `Abstract` text NOT NULL,
  `Volume` varchar(255) NOT NULL,
  `Issue` varchar(255) NOT NULL,
  `Page` varchar(255) NOT NULL,
  `CAuthor` varchar(255) NOT NULL,
  `Year` varchar(255) NOT NULL,
  `Theme` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `StaffID` (`StaffID`),
  KEY `StaffID_2` (`StaffID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_publication`
--

INSERT INTO `tbl_publication` (`Id`, `StaffID`, `Category`, `Title`, `Publisher`, `Abstract`, `Volume`, `Issue`, `Page`, `CAuthor`, `Year`, `Theme`) VALUES
(1, 65, '1', 'Leveraging on hybrid ICTs for Improved Government Transparency, Accountability and Citizen Engagement', 'IEEE', '<p>MOBILE BASED TOOLS</p>', '3', '4', '6-9', 'Steven Eduard', '2015', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_report`
--

CREATE TABLE IF NOT EXISTS `tbl_report` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Subject` varchar(255) NOT NULL,
  `Message` text NOT NULL,
  `FeedBack` text NOT NULL,
  `Subscribe` int(11) NOT NULL DEFAULT '1',
  `ReportDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_report`
--

INSERT INTO `tbl_report` (`Id`, `Name`, `Email`, `Subject`, `Message`, `FeedBack`, `Subscribe`, `ReportDate`, `Status`) VALUES
(1, 'DEO SHAO', 'deoshayo@hotmail.com', 'kuna dawa feki huku manyoni', 'Nimeona mabox ya dawa feki yakiiingizwa', '<p>Tumefuatilia hakuna ukweli wowote</p>', 1, '2015-04-13 04:57:41', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_research_group`
--

CREATE TABLE IF NOT EXISTS `tbl_research_group` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `WGID` int(11) NOT NULL,
  `staffId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `WGID` (`WGID`),
  KEY `staffId` (`staffId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_research_group`
--

INSERT INTO `tbl_research_group` (`Id`, `WGID`, `staffId`) VALUES
(10, 1, 65),
(11, 2, 91),
(12, 1, 70),
(13, 1, 126),
(14, 2, 126);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_research_theme`
--

CREATE TABLE IF NOT EXISTS `tbl_research_theme` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `Biography` text NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_research_theme`
--

INSERT INTO `tbl_research_theme` (`Id`, `Name`, `Description`, `Biography`) VALUES
(1, 'ICT4Health', '<p>Applications of ICT for health</p>', '<p>The ICT4Health Community has moved beyond the focus of whether information, communication and technology is beneficial within the health sector. Globally there has been acceptance of technology in all spheres including health. Research in this area will continue to be ongoing as technology evolves. In the ICT for health environment there is no shortage of innovation. &nbsp;The challenge is to integrate innovative scalable and sustainable solutions into the health system.</p>\r\n<p>The 2014 ICT4Health conference will be an opportunity for the various sectors including government, healthcare and IT industry to present, discuss and collaborate about the integration of innovative scalable and sustainable solutions. &nbsp;Day one will incorporate the Global Telehealth 2014 conference which will offer a plenary track of presentations of submitted, peer reviewed technical papers. Day two will follow the format of past ICT4Health conferences and be interactive with invited speakers&nbsp;focusing&nbsp;on the conference theme.&nbsp; A special feature of the meeting will be&nbsp;presentations concerning mobile for maternal and child health.</p>\r\n<p>Original unpublished contributions in all fields of ICT4Health and related areas of eHealth are invited. These may be offered as Full Papers (maximum 8 pages or approx. 4000 words, not previously published) subject to expert peer review by an international program committee of technical experts, or as extended abstract Short Papers (maximum 2 pages or approx. 1000 words) subject to internal review by the&nbsp;organizers. Submissions must contain clear aims, methods, results and conclusions&nbsp;for the work reported. Papers will be judged on originality, significance, technical quality, relevance to the conference, and presentation. Submission of a Full Paper or Short Paper will be regarded as an undertaking that, should it be accepted, at least one author will register and attend the conference to present the work. Submissions should be made by following the instructions on the conference website, using the style guide provided.&nbsp;</p>'),
(2, 'ICT4EDUtion', '<p>Applications of ICT for Education</p>', '<p>We are dedicated for this things</p>'),
(3, 'ICT4AG', '<p>ICT for Kilimo</p>', '<p>KILIMO KWANZA</p>'),
(4, 'ICT4GOV', '<p>ICT for government</p>', '<p>egovernment</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_school`
--

CREATE TABLE IF NOT EXISTS `tbl_school` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `Dean` int(11) NOT NULL,
  `CollegeID` int(11) NOT NULL DEFAULT '1',
  `Email` varchar(255) NOT NULL,
  `MobileNo` varchar(255) NOT NULL,
  `Biography` text NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_school`
--

INSERT INTO `tbl_school` (`Id`, `Name`, `Description`, `Dean`, `CollegeID`, `Email`, `MobileNo`, `Biography`, `Status`) VALUES
(1, 'SOI', '<p>School of Informatics</p>', 65, 1, 'dean.soi@udom.ac.tz', '0758749476', '<p>Welcome to the School of Informatics of the University of Dodoma. The School of Informatics is expecting to support the College of Informatics and Virtual Education and University of Dodoma to realise the vision of being Centre of Excellence in Information and Communication Technology. The School is striving to build competences in various areas of ICT. The School of Informatics is among of the first Schools at the University of Dodoma, it was established in 2007. Since its establishment the School has distinguished itself to come up with various innovative Degree, <strong>Diplom</strong>a and Certificate programmes.</p>', 1),
(2, 'SOVE', '<p>School of Virtual Education</p>', 126, 1, 'dean.sove@udom.ac.tz', '0758749475', '<p>Welcome to the School of Informatics of the University of Dodoma. The School of Informatics is expecting to support the College of Informatics and Virtual Education and University of Dodoma to realise the vision of being Centre of Excellence in Information and Communication Technology. The School is striving to build competences in various areas of ICT. The School of Informatics is among of the first Schools at the University of Dodoma, it was established in 2007. Since its establishment the School has distinguished itself to come up with various innovative Degree, Diploma and Certificate programmes.</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff`
--

CREATE TABLE IF NOT EXISTS `tbl_staff` (
  `staffId` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) NOT NULL,
  `middleName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `Position` int(11) NOT NULL,
  `Designation` int(11) NOT NULL,
  `DepartmentID` int(11) NOT NULL,
  `Address` int(255) NOT NULL,
  `MobileNo1` varchar(255) NOT NULL,
  `MobileNo2` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Sex` varchar(10) NOT NULL,
  `Biography` text NOT NULL,
  `Picture` varchar(255) NOT NULL DEFAULT 'default',
  `password` varchar(200) NOT NULL,
  `isBlocked` int(11) NOT NULL DEFAULT '0',
  `priorityId` int(11) NOT NULL DEFAULT '2',
  `user_last_login` datetime NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`staffId`),
  KEY `priorityId` (`priorityId`),
  KEY `priorityId_2` (`priorityId`),
  KEY `Position` (`Position`),
  KEY `Designation` (`Designation`),
  KEY `staffId` (`staffId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=127 ;

--
-- Dumping data for table `tbl_staff`
--

INSERT INTO `tbl_staff` (`staffId`, `firstName`, `middleName`, `lastName`, `Position`, `Designation`, `DepartmentID`, `Address`, `MobileNo1`, `MobileNo2`, `Email`, `Sex`, `Biography`, `Picture`, `password`, `isBlocked`, `priorityId`, `user_last_login`, `Status`) VALUES
(65, 'Johanes', 'Steven', 'Sungura', 3, 5, 1, 0, '6865434', '67877677', 'steve@steve.com', 'Male', '<p>o yss</p>', '2015-HT-9534.jpg', '202cb962ac59075b964b07152d234b70', 0, 1, '2015-05-20 22:05:02', 1),
(126, 'Deo', 'Donald', 'Shao', 4, 1, 2, 0, '343443', '9976548887', 'deo@deo.com', 'Male', '<p>Loves code</p>', '2015-HT-5539.jpg', '202cb962ac59075b964b07152d234b70', 0, 2, '2015-05-20 17:43:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff_position`
--

CREATE TABLE IF NOT EXISTS `tbl_staff_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_staff_position`
--

INSERT INTO `tbl_staff_position` (`id`, `position`, `description`) VALUES
(1, 'Tutorial Assistant', ''),
(2, 'Assistant Lecturer', ''),
(3, 'Lecturer', ''),
(4, 'Senior Lecturer', ''),
(5, 'Professor', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_study_level`
--

CREATE TABLE IF NOT EXISTS `tbl_study_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `levelName` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_study_level`
--

INSERT INTO `tbl_study_level` (`id`, `levelName`, `description`) VALUES
(1, 'Certificate', ''),
(2, 'Diploma', ''),
(3, 'Bachelor', ''),
(4, 'Masters', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_submodule`
--

CREATE TABLE IF NOT EXISTS `tbl_submodule` (
  `submodID` int(11) NOT NULL AUTO_INCREMENT,
  `parentID` int(11) NOT NULL,
  `submodName` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `submodTitle` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `subUrl` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Shows` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`submodID`),
  KEY `parentID` (`parentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=75 ;

--
-- Dumping data for table `tbl_submodule`
--

INSERT INTO `tbl_submodule` (`submodID`, `parentID`, `submodName`, `submodTitle`, `subUrl`, `Shows`) VALUES
(1, 1, 'Blog', 'Blog', 'blog', 1),
(8, 1, 'blog_item', 'blog_item', 'blog_item', 1),
(9, 1, 'services', 'services', 'services', 1),
(10, 1, 'service_profile', 'service_profile', 'service_profile', 1),
(11, 1, 'contact', 'contact', 'contact', 1),
(12, 10, 'Add Article', 'Article', 'article', 1),
(13, 10, 'Article List', 'article list', 'article_list', 1),
(14, 9, 'Upload Sections', 'Translations', 'upload_dictionary', 1),
(15, 9, 'Sections', 'Translation', 'dictionary', 1),
(17, 5, 'Module Edit', 'Mudule Edit', 'module_edit', 0),
(18, 5, 'Add Module', 'Framework module', 'framework_module', 1),
(19, 5, 'Add Page', 'Framework Page', 'framework_page', 1),
(20, 5, 'Manage Page', 'Framework Show', 'framework_show', 1),
(21, 4, 'Back up', 'Back Up', 'backup', 1),
(22, 4, 'Block IP', 'Block IP', 'block_ip', 1),
(23, 4, 'Audit Trails', 'Audit Trails', 'log_history', 1),
(27, 4, 'Add User', 'user_account', 'user_account', 1),
(28, 4, 'User Roles', 'user_privilege', 'user_privilege', 1),
(29, 4, 'Institution', 'Institution', 'Institution', 1),
(30, 9, 'Labels', 'Portal Label', 'dictionary_labels', 1),
(31, 9, 'Upload Labels', 'Upload Labels', 'upload_dictionary_labels', 1),
(35, 12, 'Profile List', 'Profile List', 'drug_profile_list', 1),
(37, 13, 'Reports', '<p>Reported cases</p>', 'case_list', 1),
(38, 2, 'schooLprofile', '<p>schooLprofile</p>', 'school_profile', 1),
(39, 2, 'department_profile', '<p>department_profile</p>', 'department_profile', 1),
(40, 18, 'My CV', '<p>staff_profile</p>', 'staff_profile', 1),
(41, 2, 'programme_profile', '<p>programme_profile</p>', 'programme_profile', 1),
(42, 2, 'degree', '<p>degree</p>', 'degree', 1),
(43, 2, 'student_projects', '<p>student_projects</p>', 'student_projects', 1),
(44, 2, 'research', '<p>research</p>', 'research', 1),
(45, 2, 'theme_profile', '<p>theme_profile</p>', 'theme_profile', 1),
(46, 2, 'publication', '<p>publication</p>', 'publication', 1),
(47, 2, 'alumn', '<p>alumn</p>', 'alumn', 1),
(48, 2, 'about', '<p>about</p>', 'about', 1),
(49, 2, 'campuslife', '<p>campuslife</p>', 'campuslife', 1),
(50, 2, 'search_engine', '<p>search_engine</p>', 'search_engine', 1),
(51, 14, 'Add Publication', '<p>publication_min</p>', 'publication_min', 0),
(52, 14, 'My publications', '<p>My publications</p>', 'publication_list', 1),
(53, 15, 'Add School', 'Add School', 'school', 0),
(54, 15, 'School List', 'School List', 'school_list', 1),
(56, 16, 'Department List', '<p>department_list</p>', 'department_list', 1),
(57, 11, 'Programme List', '<p>programe_list</p>', 'programe_list', 1),
(58, 11, 'Course List', '<p>Course List</p>', 'course_list', 1),
(59, 11, 'Upload Courses', '<p>upload_courses</p>', 'upload_courses', 1),
(60, 11, 'Curriculum', '<p>Curriculum</p>', 'curriculum', 1),
(61, 11, 'Upload Curiculum', '<p>Curiculum</p>', 'upload_curiculum', 1),
(62, 17, 'Add Staff', '<p>staff</p>', 'staff', 0),
(63, 17, 'Edit Staff Details', '<p>staff_edit</p>', 'staff_edit', 0),
(65, 17, 'Staff List', '<p>Staff List</p>', 'staff_list', 1),
(66, 17, 'Upload Staff', '<p>Upload Staff</p>', 'upload_staff', 1),
(67, 10, 'Article Category', '<p>Article Category</p>', 'article_category', 1),
(68, 19, 'Alum List', '<p>Alumn List</p>', 'alumn_list', 1),
(69, 20, 'Research Themes', '<p>research_theme</p>', 'research_theme', 1),
(70, 22, 'FYP', '<p>Final Year Projects</p>', 'fyp_project', 1),
(71, 2, 'Final Year Projects', '<p>FYP</p>', 'fyp', 1),
(72, 2, 'Final Year Projects Item', '<p>FYP</p>', 'fyp_item', 1),
(73, 23, 'Cordinator List', '<p>coordinator_list</p>', 'coordinator_list', 1),
(74, 2, 'facility', '<p>facility</p>', 'facility', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_template`
--

CREATE TABLE IF NOT EXISTS `tbl_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template` varchar(200) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_template`
--

INSERT INTO `tbl_template` (`id`, `template`, `status`) VALUES
(2, 'civetovuti', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_priority`
--

CREATE TABLE IF NOT EXISTS `tbl_user_priority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `priority_name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_user_priority`
--

INSERT INTO `tbl_user_priority` (`id`, `priority_name`, `description`) VALUES
(1, 'Admin', ''),
(2, 'Normal Staff', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_working_group`
--

CREATE TABLE IF NOT EXISTS `tbl_working_group` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `ThemeID` int(11) NOT NULL,
  `Image` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `PostDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Report` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `ThemeID` (`ThemeID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_working_group`
--

INSERT INTO `tbl_working_group` (`Id`, `Name`, `Description`, `ThemeID`, `Image`, `PostDate`, `Report`) VALUES
(1, 'Applying Mobile Technology for Medical Drugss Verifcation', '<p>Accountable government recognizes the needs and interests of citizens and works to advance public welfare. Lack of accountability impedes both democracy and socio-economic development of any communit<strong>y. Citizen</strong> feedback is important for boosting government transparency and accountability. ICTs can help expanding the communication channel between government and citizen while at the same time foster transparency and accountability in the public services. This study proposes the use of hybrid ICTs as a tool to engage citizen in monitoring their government. Evaluation of the proposed prototype shows the use of ICTs in facilitating citizen engagement is still infancy and more research are needed to explore their feasibility in Tanzanian <strong>context.</strong></p>', 1, '', '2015-04-29 09:17:21', ''),
(2, 'Applying Mobile Technology for Medical Drugs Verifcation', 'Accountable government recognizes the needs and interests of citizens and works to advance public welfare. Lack of accountability impedes both democracy and socio-economic development of any community. Citizen feedback is important for boosting government transparency and accountability. ICTs can help expanding the communication channel between government and citizen while at the same time foster transparency and accountability in the public services. This study proposes the use of hybrid ICTs as a tool to engage citizen in monitoring their government. Evaluation of the proposed prototype shows the use of ICTs in facilitating citizen engagement is still infancy and more research are needed to explore their feasibility in Tanzanian context. ', 1, '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_workload_setting`
--

CREATE TABLE IF NOT EXISTS `tbl_workload_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `status` int(11) NOT NULL COMMENT 'on:1;off:0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_workload_setting`
--

INSERT INTO `tbl_workload_setting` (`id`, `name`, `description`, `status`) VALUES
(1, 'CATACHOOSEANYCOURSE', 'Allow TA to choose any course', 0),
(2, 'ALLOWEXTRAWORKLOAD', 'Allow staff to add extra courses', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work_policy`
--

CREATE TABLE IF NOT EXISTS `tbl_work_policy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `positionID` int(11) NOT NULL,
  `minCourse` int(11) NOT NULL COMMENT 'minimum number of courses as chief',
  `maxCourse` int(11) NOT NULL COMMENT 'maxmum number of course as Chief',
  `assCourse` int(11) NOT NULL COMMENT 'course to assist',
  PRIMARY KEY (`id`),
  KEY `positionID` (`positionID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_work_policy`
--

INSERT INTO `tbl_work_policy` (`id`, `positionID`, `minCourse`, `maxCourse`, `assCourse`) VALUES
(1, 1, 0, 4, 3),
(2, 1, 1, 4, 2),
(3, 1, 2, 4, 0),
(4, 2, 2, 4, 0),
(5, 3, 2, 4, 0),
(6, 4, 2, 4, 0),
(7, 5, 2, 4, 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_cordinator`
--
ALTER TABLE `tbl_cordinator`
  ADD CONSTRAINT `tbl_cordinator_ibfk_1` FOREIGN KEY (`StudyLevel`) REFERENCES `tbl_study_level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_cordinator_ibfk_2` FOREIGN KEY (`StaffID`) REFERENCES `tbl_staff` (`staffId`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `tbl_course_selection`
--
ALTER TABLE `tbl_course_selection`
  ADD CONSTRAINT `tbl_course_selection_ibfk_1` FOREIGN KEY (`courseID`) REFERENCES `tbl_course` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_course_selection_ibfk_2` FOREIGN KEY (`academicID`) REFERENCES `tbl_academic_year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_course_selection_ibfk_3` FOREIGN KEY (`staffID`) REFERENCES `tbl_staff` (`staffId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_curiculum`
--
ALTER TABLE `tbl_curiculum`
  ADD CONSTRAINT `tbl_curiculum_ibfk_1` FOREIGN KEY (`ProgramID`) REFERENCES `tbl_program` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_curiculum_ibfk_2` FOREIGN KEY (`CourseID`) REFERENCES `tbl_course` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_department`
--
ALTER TABLE `tbl_department`
  ADD CONSTRAINT `tbl_department_ibfk_1` FOREIGN KEY (`SchoolID`) REFERENCES `tbl_school` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_fyp_group`
--
ALTER TABLE `tbl_fyp_group`
  ADD CONSTRAINT `tbl_fyp_group_ibfk_1` FOREIGN KEY (`ProjectID`) REFERENCES `tbl_fyp_project` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_fyp_group_ibfk_2` FOREIGN KEY (`StudentID`) REFERENCES `tbl_fyp_student` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_fyp_project`
--
ALTER TABLE `tbl_fyp_project`
  ADD CONSTRAINT `tbl_fyp_project_ibfk_1` FOREIGN KEY (`Supervisor`) REFERENCES `tbl_staff` (`staffId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_privilege`
--
ALTER TABLE `tbl_privilege`
  ADD CONSTRAINT `tbl_privilege_ibfk_1` FOREIGN KEY (`priorityId`) REFERENCES `tbl_user_priority` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_privilege_ibfk_2` FOREIGN KEY (`ModuleID`) REFERENCES `tbl_module` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_program`
--
ALTER TABLE `tbl_program`
  ADD CONSTRAINT `tbl_program_ibfk_1` FOREIGN KEY (`StudyLevel`) REFERENCES `tbl_study_level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_publication`
--
ALTER TABLE `tbl_publication`
  ADD CONSTRAINT `tbl_publication_ibfk_1` FOREIGN KEY (`StaffID`) REFERENCES `tbl_staff` (`staffId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD CONSTRAINT `tbl_staff_ibfk_1` FOREIGN KEY (`priorityId`) REFERENCES `tbl_user_priority` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_staff_ibfk_3` FOREIGN KEY (`Designation`) REFERENCES `tbl_designation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_staff_ibfk_4` FOREIGN KEY (`Position`) REFERENCES `tbl_staff_position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_work_policy`
--
ALTER TABLE `tbl_work_policy`
  ADD CONSTRAINT `tbl_work_policy_ibfk_1` FOREIGN KEY (`positionID`) REFERENCES `tbl_staff_position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
