
    
    	<div class="container"><!-- container -->
        
        	<div class="row"><!-- row -->
            
                <div id="k-top-search" class="col-lg-12 clearfix"><!-- top search -->

                </div><!-- top search end -->
            
            	<div class="k-breadcrumbs col-lg-12 clearfix"><!-- breadcrumbs -->
                
                    
                </div><!-- breadcrumbs end -->               
                
            </div><!-- row end -->
            
            <div class="row no-gutter fullwidth"><!-- row -->
                
                <div class="col-lg-12 col-md-12"><!-- doc body wrapper -->
                	
                    <div class="col-padded"><!-- inner custom column -->
                    

                        <h1 class="page-title">About CIVE</h1>
                        
                        <div class="news-body">
                        
                            <p class="call-out">
<?php about_college (1); ?>
			    </p>
                        

                            
                            <div class="row gutter k-equal-height"><!-- row -->
                            
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <h6>Facilities</h6><p>
<?php facilities_default (); ?>
                                </p></div>
                                
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <h6>Campus Life</h6><p>
<?php campuslife_default ();?>
                                </p></div>
                                
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <h6>Our students</h6>
                                    <p>
<?php
our_students ();
?>
				    </p>
                                </div>
                            
                            </div><!-- row end -->
                            
                        </div>
                    
                    </div><!-- inner custom column end -->
                    
                </div><!-- doc body wrapper end -->
            
            </div><!-- row end -->
        
        </div><!-- container end -->
