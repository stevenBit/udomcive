<header class="navbar" id="top" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--
            <div class="navbar-brand nav" id="brand">
                <a href="index.html"><img src="assets/img/civepics/udom_logo.jpg" alt="brand" class="img-responsive"></a>
            </div>
            -->

        </div>
        <?php
        //urls
            $soi="index.php?q=school_profile&s=1";
            $sove="index.php?q=school_profile&s=2";
            $non_degree="index.php?q=degree&d=Certificate";
            $Diploma="index.php?q=degree&d=Diploma";
            $degree="index.php?q=degree&d=Bachelor";
            $master="index.php?q=degree&d=Masters";
            $blog="index.php?q=blog";
            $facility="index.php?q=facility&d=";
            $alumn="index.php?q=alumn";
            $about="about"; //index.php?q=about
            $campuslife="index.php?q=campuslife&d=";
            $fyp="index.php?q=student_projects";
            $publication="index.php?q=publication";
            $research="index.php?q=research&d=ResearchSection";
            $home="index.php";

        ?>
        <!--
        <img src="<?php echo WEB_ROOT;?>assets/img/udom_logo.jpg" alt="brand" class="float-left">
        -->
        <h2>College of of Informatics and Virtual Education</h2>
        <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="<?php echo $home; ?>" class="has-child no-link">Home</a>
                    <ul class="list-unstyled child-navigation">
                        <li><a href="<?php echo $about; ?>">About</a></li>

                    </ul>
                </li>

                <li>
                    <a href="#" class="has-child no-link">About Us</a>
                    <ul class="list-unstyled child-navigation">
                        <li><a href="<?php echo $about; ?>">Campus Life</a></li>
                        <li><a href="<?php echo $facility; ?>">Facilities</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class=" has-child no-link">Schools</a>
                    <ul class="list-unstyled child-navigation">
                        <li><a href="<?php echo $soi; ?>">Schools of Informatics</a></li>
                        <li><a href="<?php echo $sove; ?>">Schools of Virtual Education</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="has-child no-link">Admission</a>
                    <ul class="list-unstyled child-navigation">
                        <li><a href="<?php echo $non_degree; ?>">Certificates</a></li>
                        <li><a href="<?php echo $Diploma; ?>">Diploma</a></li>
                        <li><a href="<?php echo $degree; ?>">Bachelor</a></li>
                        <li><a href="<?php echo $master; ?>">Postgraduate</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="has-child no-link">Research</a>
                    <ul class="list-unstyled child-navigation">
                        <li><a href="<?php echo $publication; ?>">Publication</a></li>
                        <li><a href="<?php echo $publication; ?>">Journals</a></li>
                        <li><a href="<?php echo $publication; ?>">Projects</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo $blog; ?>">Blog</a>
                </li>
            </ul>
        </nav><!-- /.navbar collapse-->
    </div><!-- /.container -->
</header><!-- /.navbar -->