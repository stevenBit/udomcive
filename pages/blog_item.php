
<?php

function article_tag()
{
    $tag=mysql_query("select * from tbl_article_category");
    echo "<ul class='list-unstyled blog-tags margin-bottom-30'>"; 
    while ($r=mysql_fetch_array($tag))
    {
        $Name=$r['Name'];
        $did=$r['Id'];
        $self="index.php?q=blog&cat=".$did;
        echo"<li><a href='$self'><i class='fa fa-tags'></i>$Name</a></li>";
    }
    echo "</ul>"; 
}


function article_tag_id($id)
{
    $tag=mysql_query("select c.Name as Name, c.Id from tbl_article_category c, tbl_article a where a.Category=c.Id and a.Id='$id'");
    echo "<ul class='list-unstyled list-inline blog-tags'>"; 
    while ($r=mysql_fetch_array($tag))
    {
        $Name=$r['Name'];
        $did=$r['Id'];
        $self="index.php?q=blog&cat=".$did;
        echo"<li><i class='fa fa-tags'></i> <a href='$self'>$Name</a> </li>";
    }
    echo "</ul>"; 
}


            if(isset ($_GET['action']))
            {
              $did=@$_GET['action'];
	     
	            if($did=='p')
	            {
		            $article=mysql_query("select * from tbl_article where Category='8' order by PostDate DESC");
	      
	             }else {
                    $article=mysql_query("select * from tbl_article where Id='$did'") or die (mysql_error());
	      
	            }
	        }elseif(isset ($_GET['id'])){
                    $did=@$_GET['id'];
                    $article=mysql_query("select * from tbl_article where Id='$did'") or die (mysql_error());
	      
            }else{
		
		            $article=mysql_query("select * from tbl_article order by PostDate DESC") or die (mysql_error());
            
	        }
             
            
           
            $r=mysql_fetch_array($article);
            $content=$r['Content'];
            $content=strip_tags($content);
            $title=strip_tags($r['Title']);
            $date1=$r['PostDate'];
            $PostDate=$r['PostDate'];
            $date = ""; //date("d-M-Y",$date1);
            $image=$r['Image'];

                if(($image=="article.jpg") || ($image==""))
                {
                    $image="";
                }else{
                    $image= WEB_ROOT."/images/articlePictures/".$image;
                }
            
    ?>
    

<!-- Page Content -->
<div id="page-content">
    <div class="container">
        <div class="row">
            <!--MAIN Content-->
            <div class="col-md-8">
                <div id="page-main">
                    <section id="blog-detail">
                        <header><h1>Blog / News</h1></header>
                        <article class="blog-detail">
                            <header class="blog-detail-header">
                                <img class="img-responsive" src="<?php echo $image;?>" alt="">
                                <h2><?php echo $title;?></h2>
                                <div class="blog-detail-meta">
                                    <span class="date"><span class="fa fa-calendar"></span><?php echo "Posted : ".$PostDate; ?></span>
                                </div>
                            </header>
                            <hr>
                            <p>
                            <?php echo $content;?>
                            </p>

                        </article>
                    </section><!-- /.blog-detail -->

                    <hr>

                </div><!-- /#page-main -->
            </div><!-- /.col-md-8 -->

            <!--SIDEBAR Content-->
            <div class="col-md-4">
                <div id="page-sidebar" class="sidebar">
                    <aside class="news-small" id="news-small">
                        <header>
                            <h2>Recent Posts</h2>
                        </header>
                        <div class="section-content">
                        <?php recent_article(); ?>
                        </div><!-- /.section-content -->

                    </aside><!-- /.news-small -->


                </div><!-- /#sidebar -->
            </div><!-- /.col-md-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>