    <div id="k-body"><!-- content wrapper -->
    
    	<div class="container"><!-- container -->
        

            
            <div class="row no-gutter"><!-- row -->
                
                <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->
                	
                    <div class="col-padded"><!-- inner custom column -->
                    
                    	<div class="row gutter"><!-- row -->
                        
                        	<div class="col-lg-12 col-md-12">
                    
                                <figure class="news-featured-image">	
                                    <img src="<?php echo WEB_ROOT;?>templates/cive/img/civehome.jpg" alt="Featured image 1" class="img-responsive" />
                                </figure>
                                 <?php
				 principal_message (800);

				 ?>
                            </div>
                        
                        </div><!-- row end -->
                        
                        <div class="row gutter k-equal-height"><!-- row -->
                        
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h6>Announcements</h6>
                                <p>
                               <?php                     
				   announcement ();
			       ?>
				</p>
                            </div>
                            
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h6> News </h6>
                                <p>
				   <?php 
				   news (80);
				   ?>
                                </p>
                            </div>
                        
                        </div><!-- row end -->           
                    
                    </div><!-- inner custom column end -->
                    
                </div><!-- doc body wrapper end -->
                
                <div id="k-sidebar" class="col-lg-4 col-md-4"><!-- sidebar wrapper -->
                	
                  <?php sidebar(); ?>
		
		</div><!-- sidebar wrapper end -->
            
            </div><!-- row end -->
        
        </div><!-- container end -->
    
    </div><!-- content wrapper end -->