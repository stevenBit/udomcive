<header class="navbar" id="top" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--
            <div class="navbar-brand nav" id="brand">
                <a href="index.html"><img src="assets/img/civepics/udom_logo.jpg" alt="brand" class="img-responsive"></a>
            </div>
            -->

        </div>
        <img src="assets/img/civepics/udom_logo.jpg" alt="brand" class="float-left"> College of of Informatics and Virtual Education
        <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="#" class="has-child no-link">Home</a>
                    <ul class="list-unstyled child-navigation">
                        <li><a href="index.html">Homepage Education</a></li>
                        <li><a href="homepage-courses.html">Homepage Courses</a></li>
                        <li><a href="homepage-events.html">Homepage Events</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#" class="has-child no-link">About Us</a>
                    <ul class="list-unstyled child-navigation">
                        <li><a href="event-listing-images.html">Campus Life</a></li>
                        <li><a href="event-listing.html">Facilities</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class=" has-child no-link">Schools</a>
                    <ul class="list-unstyled child-navigation">
                        <li><a href="course-landing-page.html">Schools of Informatics</a></li>
                        <li><a href="course-listing.html">Schools of Virtual Education</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="has-child no-link">Admission</a>
                    <ul class="list-unstyled child-navigation">
                        <li><a href="blog-listing.html">Certificates</a></li>
                        <li><a href="blog-detail.html">Diploma</a></li>
                        <li><a href="blog-detail.html">Bachelor</a></li>
                        <li><a href="blog-detail.html">Postgraduate</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="has-child no-link">Research</a>
                    <ul class="list-unstyled child-navigation">
                        <li><a href="full-width.html">Publication</a></li>
                        <li><a href="left-sidebar.html">Journals</a></li>
                        <li><a href="right-sidebar.html">Projects</a></li>
                    </ul>
                </li>
                <li>
                    <a href="contact-us.html">Contact Us</a>
                </li>
            </ul>
        </nav><!-- /.navbar collapse-->
    </div><!-- /.container -->
</header><!-- /.navbar -->