<?php
//Self Reference
require_once '../library/config.php';
require_once '../library/functions.php';

require_once("excel_reader2.php");
//$self="index.php?q=". MD5('importEmployee');
$self="index.php?q=upload_curiculum";
//include 'db.php';

$token = (isset($_GET['token']) && $_GET['token'] != '') ? $_GET['token'] : '';

//messages
if($token == 'okay'){
	//echo "<p class='message'>File records Import Finished Successfully</p>";
	upload_file_sucess();
} else if($token == 'not'){
	//echo "<p class='message'>File records Import Finished UnSuccessfully  $_GET[err] record(s) rejected</p>";
	uploadError("File records Import Finished UnSuccessfully  $_GET[err] record(s) rejected");
} else if ( $token == 'fail'){
	echo "<p class='message'>There is an internal error only $_GET[err] row(s) imported</p>";
}

//import translations details
if(!empty($_FILES['userfile']))
{
	# delete old files
	//@unlink("temp/ResultFile.txt");
	#constants
	$fileavailable=0;
	#validate the file
	$file_name = addslashes(@$_POST['userfile']);
	$overwrite = addslashes(@$_POST['radiobutton']);
	#check file extension
	$file = @$_FILES['userfile']['name'];
	//The original name of the file on the client machine. 
	$filetype = @$_FILES['userfile']['type'];
	//The mime type of the file, if the browser provided this information. An example would be "image/gif". 
	$filesize = @$_FILES['userfile']['size'];
	//The size, in bytes, of the uploaded file. 
	$filetmp = @$_FILES['userfile']['tmp_name'];
	//The temporary filename of the file in which the uploaded file was stored on the server. 
	$filetype_error = @$_FILES['userfile']['error'];
	$filename=time().@$_FILES['userfile']['name'];
	// In PHP earlier then 4.1.0, $HTTP_POST_FILES  should be used instead of $_FILES.
	$rejectedArray = array();
	if (is_uploaded_file($filetmp))
	{
		$filename=time().$file;
		copy($filetmp, "temp/$filename");
		move_uploaded_file($filetmp,"temp/$filename");
		$str = $filename;
		$i = strrpos($str,".");
		if (!$i) 
		{
			return "";
		}
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		$pext = strtolower($ext);
		if ($pext != "xls")
		{
			print "<p class='message'>File Extension Unknown.<br>";
			unlink("temp/$filename");
		} else {
			#unzip the file first
			if ($pext == "xls") 
			{
				$fileavailable=1;
			}
			#STEP 2.0 exceute sql scripts
			if ($fileavailable==1)
			{
				$data = new Spreadsheet_Excel_Reader("temp/$filename");
				$rows = $data->rowcount(0);
				$error=0;
				$sucess=0;
				$title = 0;
				$fcontents = file ("temp/$filename"); 
				# expects the csv file to be in the same dir as this script
				for($i=2; $i<= $rows; $i++) 
				{ 
					
					$CourseIDE   = $data->val($i,2);
					$ProgramIDE  = $data->val($i,3);
					$YearOfStudy = $data->val($i,4);
					$Semester    = $data->val($i,5);
					$Core        = $data->val($i,6);
					
					if(!empty($CourseIDE) and !empty($ProgramIDE)){
						//check if records exists
						$check1      = mysql_query("select * from tbl_course where Name='$CourseIDE'");
						$n1          = mysql_num_rows($check1);
						$check2      = mysql_query("select * from tbl_program where acronym='$ProgramIDE'");
						$n2          = mysql_num_rows($check2);

						if($n1==0 || $n2==0)
						{	
							//noma ("Please enter registered course and programs only");	
							$error++;
							if($n1==0){
							$errorArray[] = array(
										'CourseID'  =>  	$CourseIDE,		  
										'ProgramID'	=>		$ProgramIDE,
										'Comment'	=>		'Course code does not exist'
										);
							}
							if($n2==0){
							$errorArray[] = array(
										'CourseID'  =>  	$CourseIDE,		  
										'ProgramID'	=>		$ProgramIDE,
										'Comment'	=>		'Programme code does not exist'
										);
							}
						}else{
							$CourseID  = retrieve_courseid($CourseIDE);
							$ProgramID = retrieve_programid($ProgramIDE);
							if ($overwrite==1)
							{
								//check if exists
								$sqlcheck = "SELECT * FROM tbl_curiculum WHERE 
											ProgramID='$ProgramID' AND 
											CourseID='$CourseID' AND 
											YearOfStudy='$YearOfStudy' AND 
											Semister='$Semester' AND 
											Core='$Core'";
								$result = mysql_query($sqlcheck);
								if(mysql_num_rows($result)>0){
									$data2=1;
								}else{
									$sql = "REPLACE INTO tbl_curiculum(ProgramID,CourseID,YearOfStudy,Semister,Core)
									VALUES ('$ProgramID','$CourseID','$YearOfStudy','$Semester','$Core')";
									$data2=mysql_query($sql) or die (mysql_error());
								}
							}else
							{
								//$sql = "INSERT INTO tbl_employee VALUES ('".implode("','", $arr) ."')"; 
								//check if the record exists
								$sqlcheck = "SELECT * FROM tbl_curiculum WHERE 
											ProgramID='$ProgramID' AND 
											CourseID='$CourseID' AND 
											YearOfStudy='$YearOfStudy' AND 
											Semister='$Semester' AND 
											Core='$Core'";
								$result = mysql_query($sqlcheck);
								if(mysql_num_rows($result)>0){
									$data2=0;
								}else{
									$sql = "INSERT INTO tbl_curiculum(ProgramID,CourseID,YearOfStudy,Semister,Core)
									VALUES ('$ProgramID','$CourseID','$YearOfStudy','$Semester','$Core')";
									$data2=mysql_query($sql) or die (mysql_error());
								}
							}
							
							if($data2) 
							{
								$sucess++;
							}
							else
							{
								$error++;
								$errorArray[] = array(
										'CourseID'  =>  	$CourseIDE,		  
										'ProgramID'	=>		$ProgramIDE,
										'Comment'	=>		'Internal Error'
										);							
							}
						}
					}
				}
			}
			//Say what happened
			$all=$error+$sucess;
			//keep record b'se the file change our db
			if($sucess > 0){
			//$id = $_SESSION['sis_user_id'];
			//$sql2 = mysql_query("INSERT INTO tbl_employee_excel(user_id,name,date) VALUES('$id','$filename',NOW())");
			}
			
			if($error == 0)
			{
				mafanikio("<p class='message'>Data Import Finished Successfully</p>");
				
			}else if($sucess != 0){
				noma("Data Import Finished UnSuccessfully " . $error. " record(s) rejected. Click <a href='" .errorExcelFile($errorArray). "' target='_blank'> here</a> to view rejected records ");
				/*
				for($g=0;$g<count($rejectedArray);$g++){
					echo $rejectedArray[$g][0] . "  " . $rejectedArray[$g][1] . "<br/>";
				}
				*/
				
			}else
			{
				//echo "<p class='message'>Data Import Finished UnSuccessfully</p>";
				noma("File records Import Finished UnSuccessfully  $error record(s) rejected. Click <a href='" .errorExcelFile($errorArray). "' target='_blank'> here</a> to view rejected records ");
			}
	 	}
	}
	else 
	{
		echo "<p class='message'>File not uploaded, see the error: ".$filetype_error."</p>";
	}

}
?>