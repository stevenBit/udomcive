<?php
public function load_phrases($lang_id,$page_name) {
  $this->db->q("SELECT * FROM phrases WHERE lang_id='$lang_id' AND (phrase_type='global' OR phrase_type='$page_name')");
  while($f=$this->db->f_row()) {
   $phrases[$f['phrase_name'] = $f['phrase_text'];
  }
  
  return $phrases;
 }
 
 
 
 
 public function get_current_filename() {
   $current_file = $_SERVER["SCRIPT_NAME"];
   $parts = Explode('/', $current_file);
   $current_file = $parts[count($parts) - 1];
   $current_file = substr($current_file,0,strlen($current_file)-4);
   return $current_file;
 }
 
 
 

//fetch lang ID
$lang_id = !input::check_varchar($_GET['lang_id'],2,false) ? 'en' : $_GET['lang_id'];
DEFINE('LANG_ID',$lang_id);

//fetch phrases, global and for this page
$page_name = !input::check_varchar($_GET['page_name'],20,false) ? globals::get_current_filename() : $_GET['page_name'];
$lang = new lang($lang_id,$page_name);
?>