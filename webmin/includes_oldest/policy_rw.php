<div class="tabbable" id="tabs-783341">
	<div id='divCourseSelection'>
		<form name='frmSelection'>
	<ul class="nav nav-tabs">
		<?php
		//get the study level
		//     name        id       databaseId(studylevel) year of study
		//   Certificate    1          1                        1
		// access
		//echo $programmeArray[0][0].": ID: ".$programmeArray[0][1].", DBID: ".$programmeArray[0][2].", YOSD: ".$programmeArray[0][3].".<br>";
		$programmeArray = array
						  (
						  array("Certificate",  1,1,1),
						  array("First Year",   2,2,1),
						  array("Second Year",  3,2,2),
						  array("First Year",   4,3,1),
						  array("Second Year",  5,3,2),
						  array("Third Year",   6,3,3),
						  array("Four Year",    7,3,4),
						  array("Postgraduate", 8,4,1)
						  );					
		$sql = "SELECT id, levelName FROM tbl_study_level";
		$result = dbQuery($sql);
		
		?>
		<!--certificate -->
		<li class='active'>
			<a href="#panel-<?php echo $programmeArray[0][1]; ?>" data-toggle="tab"><?php echo $programmeArray[0][0]; ?></a>
		</li>
		 <!--DIPLOMA -->
		<li class="dropdown">
			<a data-toggle="dropdown" class="dropdown-toggle" href="#">Diploma <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a data-toggle="tab" href="#panel-<?php echo $programmeArray[1][1]; ?>"><?php echo $programmeArray[1][0]; ?></a></li>
				<li><a data-toggle="tab" href="#panel-<?php echo $programmeArray[2][1]; ?>"><?php echo $programmeArray[2][0]; ?></a></li>
			</ul>
		</li>
		<!--BACHELOR -->
		<li class="dropdown">
			<a data-toggle="dropdown" class="dropdown-toggle" href="#">Bachelor  <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li><a data-toggle="tab" href="#panel-<?php echo $programmeArray[3][1]; ?>"><?php echo $programmeArray[3][0]; ?></a></li>
				<li><a data-toggle="tab" href="#panel-<?php echo $programmeArray[4][1]; ?>"><?php echo $programmeArray[4][0]; ?></a></li>
				<li><a data-toggle="tab" href="#panel-<?php echo $programmeArray[5][1]; ?>"><?php echo $programmeArray[5][0]; ?></a></li>
				<li><a data-toggle="tab" href="#panel-<?php echo $programmeArray[6][1]; ?>"><?php echo $programmeArray[6][0]; ?></a></li>
			</ul>
		</li>
		<!--Postgraduate -->
		<li>
			<a href="#panel-<?php echo $programmeArray[7][1]; ?>" data-toggle="tab"><?php echo $programmeArray[7][0]; ?></a>
		</li>
	</ul>
	<!-- Load Courses for Program -->
	<div class="tab-content">
		<?php
			$box = 1;
			//get the staff position
			$positionID = getStaffPositionID($_SESSION['cive_user_id']);
			for($i=0; $i<count($programmeArray); $i++){
			?>
			<div class="tab-pane <?php echo($i==0)? 'active': ''; ?>" id="panel-<?php echo $programmeArray[$i][1]; ?>">
				<br/>
				<table class="table table-striped table-bordered table-hover" id="dataTables-example<?php echo $programmeArray[$i][1]; ?>">
				<thead style='font-size:12px;'>
				<tr>										
				<th width="20px">#</th>
				<th width="250px">Course Name</th>
				<th width="50px">Code</th>
				<!-- Load Programme acronyms in the level--->
				<?php
					$resultProgramme = dbQuery("SELECT Id, Name, acronym FROM tbl_program WHERE StudyLevel='{$programmeArray[$i][2]}'");
					$k=0;
					while($rowProgramme = dbFetchAssoc($resultProgramme)){
						echo"<th><a href='#' style='text-decoration: none;' title='{$rowProgramme['Name']}'>{$rowProgramme['acronym']}</a></th>";
						$dbProgrammeId[$i][$k]= $rowProgramme['Id']; //load pro
						$k++;
					}
				?>
				<th >Instr.</th>
				<th >Assist. Instr.</th>
				</tr>
				</thead>  
				<tbody>
				<!-- Load Programme courses in the level--->
				<?php
					$currentSemester = getCurrentSemester();
					$sqlloadCourse = "SELECT DISTINCT(c.Name) as Name, c.Description, c.Id 
									  FROM tbl_course c, tbl_curiculum cr, tbl_program p
									  WHERE cr.courseID=c.id AND cr.programID=p.id AND cr.Semister='$currentSemester' AND cr.YearOfStudy='{$programmeArray[$i][3]}'
									  AND p.StudyLevel='{$programmeArray[$i][2]}'";
					//echo $sqlloadCourse;				 
					$resultCourse = dbQuery($sqlloadCourse);
					if(dbNumRows($resultCourse)>0){
						$num = 1;
						while($rowCourses = dbFetchAssoc($resultCourse)){
							//ids for course selection
							$checkName    = "check_" .  $box;
							$checkHidden  = "action_" . $box;
							$checkNameDiv = "div_" . $box;
							?>
							<tr style='font-size:12px;'>
								<td><?php echo $num; ?></td>
								<td><?php echo $rowCourses['Description']; ?></td>
								<td><?php echo $rowCourses['Name']; ?></td>
								<!-- Check if the program enrolled--->
								<?php
								
								for($r=0;$r<$k;$r++){
									$sqlloadCourse2 = "SELECT Core 
													  FROM tbl_curiculum cr
													  WHERE cr.Semister='$currentSemester' AND cr.programID='{$dbProgrammeId[$i][$r]}' AND cr.courseID='{$rowCourses['Id']}'
													  AND cr.YearOfStudy='{$programmeArray[$i][3]}'";
									$resultCheckCourse = dbQuery($sqlloadCourse2);
									if(dbNumRows($resultCheckCourse)>0){
										$rowCheckCourse = dbFetchAssoc($resultCheckCourse);
										if($rowCheckCourse['Core'] == 1)
											echo "<td style='background-color:" . BGCOLORCORE . ";'><a href='#' title='Core'>&nbsp;</a></td>";
										else
											echo "<td style='background-color:" . BGCOLORELECTIVE . ";'><a href='#' title='Elective'>&nbsp;</a></td>";
									}else{
										echo "<td style='background-color:#ffffff;'>&nbsp;</td>";
									}
										
								}
								/**
									Instructor & Assistant Selection
									isCourseSelected(courseID,academicID,Status[chief 1;assist 2])
								**/
								$academic = getCurrentAcademic();
								$instructor = isCourseSelected($rowCourses['Id'],$academic,1);
								//check if Instructor can be a chief
								$workload = getworkloadSetting("CATACHOOSEANYCOURSE");
								if($positionID=='1' and $workload == '0' and $i>=3)
									$canBeChief = "disabled";
								else
									$canBeChief = "";
								if(is_array($instructor)){
									//check edit privilege
									if($_SESSION['cive_user_id'] == $instructor[0])
										$editPrivilege = "";
									else
										$editPrivilege ="disabled";
								?>
								<td  align='left'>
									<span id="<?php echo $checkNameDiv; ?>">
										<input type='checkbox' name="<?php echo $checkName; ?>" id="<?php echo $checkName; ?>" value="0" checked='checked'
										onClick="courseSelection(document.frmSelection,'courseSelection','<?php echo $checkNameDiv; ?>','<?php echo $rowCourses['Id']; ?>','<?php echo $academic ?>','<?php echo $_SESSION['cive_user_id']?>','1','<?php echo $checkHidden; ?>','<?php echo $checkName; ?>');" <?php echo $canBeChief . " " . $editPrivilege; ?>>
										<input type='hidden' name="<?php echo $checkHidden;?>" id="<?php echo $checkHidden;?>" value=0  />
										&nbsp;<?php echo $instructor[1]; ?>
									</span>
								</td>
								<?php
								} else { ?>
								<td  align='left'>
									<span id="<?php echo $checkNameDiv; ?>">
										<input type='checkbox' name="<?php echo $checkName; ?>" id="<?php echo $checkName; ?>" value="1" 
										onClick="courseSelection(document.frmSelection,'courseSelection','<?php echo $checkNameDiv; ?>','<?php echo $rowCourses['Id']; ?>','<?php echo $academic ?>','<?php echo $_SESSION['cive_user_id']?>','1','<?php echo $checkHidden; ?>','<?php echo $checkName; ?>');" <?php echo $canBeChief; ?>>
										<input type='hidden' name="<?php echo $checkHidden;?>" id="<?php echo $checkHidden;?>" value=1 />
									</span>
								</td>
								<?php 
								}
								?>
								<td><input type='checkbox' name="check2"/></td>
							</tr>
							<?php
							$num++;
							$box++;
						}
					}
				?>
				</tbody>
				</table>
				<table>
				<table>
					<tr>
						<td colspan='2'><b>KEY</b></td>
					</tr>
					<tr>
						<td style='background-color:<?php echo BGCOLORCORE; ?>; width:30px;'></td>
						<td>&nbsp;&nbsp;Core</td>
					</tr>
					<tr>
						<td style='background-color:<?php echo BGCOLORELECTIVE; ?>;'></td>
						<td>&nbsp;&nbsp;Elective</td>
					</tr>
				</table>
			</div>
			<?php
			}
		?>
	</div>
	</form>
	</div>
</div>



    
