<?php
require_once 'library/config.php';
require_once 'library/functions.php';
$loginerror = '';

if (isset($_POST['txtUserName'])) {
	$result = doLogin2();

	if ($result != '') {
		$loginerror = $result;
	}
}
$Institution_name=INSTITUTION_NAME;
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CIVE-INTRANET</title>

    <!-- Bootstrap Core CSS -->
    <link href="templates/civetovuti/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="templates/civetovuti/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="templates/civetovuti/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="templates/civetovuti/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<script type="text/javascript">
function userlogin_onsubmit ( theForm )
{
	//theForm = window.document.saveBill;
	
	if (theForm.txtUserName.value == '') {
		alert('Username field is required');
		theForm.txtUserName.focus();
		return false;
	} else if (theForm.txtPassword.value == '') {
		alert('Password field is required');
		theForm.txtPassword.focus();
		return false;
	} else {
		return true;
	}
}
</script>
	
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                        					<?php
					if (isset($loginerror ) && $loginerror !="")
					{
						noma($loginerror);
					}
					?>
	
                    </div>
                    <div class="panel-body">
                        <form method="post" nctype="multipart/form-data" name="userlogin" 
			onsubmit="return userlogin_onsubmit(this)" LANGUAGE=javascript>
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="txtUserName" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="txtPassword" type="password" value="">
                                </div>
                          
                                <!-- Change this to a button or input when using this as a form -->
                              
                                <button class="btn btn-lg btn-success btn-block" type="submit" name="Login">Sign In</button> </br></br></br>
				
				
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="templates/civetovuti/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="templates/civetovuti/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="templates/civetovuti/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="templates/civetovuti/dist/js/sb-admin-2.js"></script>

</body>

</html>
