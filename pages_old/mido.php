    <div id="k-body"><!-- content wrapper -->
    
    	<div class="container"><!-- container -->
        
        	<!--
            <div class="row">
                <div id="k-top-search" class="col-lg-12 clearfix">
                
                    <form action="#" id="top-searchform" method="get" role="search">
                        <div class="input-group">
                            <input type="text" name="s" id="sitesearch" class="form-control" autocomplete="off" placeholder="Type in keyword(s) then hit Enter on keyboard" />
                        </div>
                    </form>
                    
                    <div id="bt-toggle-search" class="search-icon text-center"><i class="s-open fa fa-search"></i><i class="s-close fa fa-times"></i></div>
                
                </div>
            
            	<div class="k-breadcrumbs col-lg-12 clearfix">
		     
                
                	<ol class="breadcrumb">
                    	<li><a href="index.html">Home</a></li>
                        <li class="active">About Us</li>
                    </ol>
                    
                </div>            
                
            </div>
		-->
            
            <div class="row no-gutter"><!-- row -->
                
                <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->
                	
                    <div class="col-padded"><!-- inner custom column -->
                    
                    	<div class="row gutter"><!-- row -->
                        
                        	<div class="col-lg-12 col-md-12">
                    
                                <figure class="news-featured-image">	
                                    <img src="<?php echo WEB_ROOT;?>templates/cive/img/civehome.jpg" alt="Featured image 1" class="img-responsive" />
                                </figure>
                                
                                <h1 class="page-title"> <?php
				
				echo dynamic_content("PrincipalNote",1);?></h1>
                                
                                <div class="news-body">
                                
                                    <p class="call-out">
                                 <?php
				 principal_message (800);
				 //echo dynamic_content_less("PrincipalNote",2,500);?>
                                    </p>

                                    
                                </div>
                            
                            </div>
                        
                        </div><!-- row end -->
                        
                        <div class="row gutter k-equal-height"><!-- row -->
                        
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h6> <?php echo portal_label("Announcement");?></h6>
                                <p>
                               <?php // dynamic_content_less("DeanSoiNote",2,500);                       
				   announcement ();
			       ?>
				</p>
                            </div>
                            
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h6> <?php echo portal_label("News");?></h6>
                                <p>
				   <?php //echo dynamic_content("DeanSoveNote",2);
				   news (4);
				   ?>
                                </p>
                            </div>
                        
                        </div><!-- row end -->           
                    
                    </div><!-- inner custom column end -->
                    
                </div><!-- doc body wrapper end -->
                
                <div id="k-sidebar" class="col-lg-4 col-md-4"><!-- sidebar wrapper -->
                	
                  <?php sidebar(); ?>
		
		</div><!-- sidebar wrapper end -->
            
            </div><!-- row end -->
        
        </div><!-- container end -->
    
    </div><!-- content wrapper end -->